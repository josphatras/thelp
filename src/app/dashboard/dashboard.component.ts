import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ChangePasswordComponent } from '../account/change-password/change-password.component';
import { Router } from '@angular/router';
import * as $ from 'jquery';

// import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SysConfig } from '../config/sys';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {
	user: any
	hearderOptions: any
	generatedHeader: any
	token: any
	items: any

  constructor(
  	private router: Router,
		private dialog: MatDialog,
		private http: HttpClient,
  ) { }

  private config = SysConfig.siteUrl; 

	readonly ROOT_URL = this.config+'api/tickets/create';
	authKey(){
		this.token = JSON.parse(localStorage.getItem('tbl_token'))
		return this.token
	}

	genHeders(token_key){
		this.generatedHeader = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json', 
				'Access-Control-Allow-Origin': '*', 
				'Authorization' : token_key,
			})
		}
		return this.generatedHeader;
	}

  ngOnInit() {
		this.user = JSON.parse(localStorage.getItem("tbl_user"));
		if (this.user == null) {
			this.router.navigate(['login']);
		}
		if (this.user.status == '0') {
	    const dialogRef = this.dialog.open(ChangePasswordComponent,{
	      disableClose: true,
	      width: '500px',
	      data: { id: this.user.id }
	    });
		}

		this.getDashboard().subscribe(
			data => {this.items = data}
		)
  }

	getDashboard(){
	  var info = this.http.get(this.config+'api/dashboard', this.genHeders('Bearer ' + this.authKey()));  
	  return info;  
	}
}
