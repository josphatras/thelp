import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SbuRoutingModule } from './sbu-routing.module';
import { ViewSbuComponent } from './view-sbu/view-sbu.component';

@NgModule({
  imports: [
    CommonModule,
    SbuRoutingModule
  ],
  declarations: [
  	// ViewSbuComponent
  ]
})
export class SbuModule { }
