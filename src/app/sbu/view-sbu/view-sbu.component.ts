import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-sbu',
  template: '<router-outlet></router-outlet>',
  styleUrls: ['./view-sbu.component.sass']
})
export class ViewSbuComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
