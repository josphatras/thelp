import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSbuComponent } from './view-sbu.component';

describe('ViewSbuComponent', () => {
  let component: ViewSbuComponent;
  let fixture: ComponentFixture<ViewSbuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewSbuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSbuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
