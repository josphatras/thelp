import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewSbuComponent } from './view-sbu/view-sbu.component';

const routes: Routes = [
	{path: 'sbu', component: ViewSbuComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SbuRoutingModule { }
