import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
	user: any
  title = 'client';
  constructor() { }

  ngOnInit() {
  }

}
