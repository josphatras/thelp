import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AccountComponent } from './account.component';
import { NewUserComponent } from './new-user/new-user.component';
import { SelectUsersComponent } from './select-users/select-users.component';
import { ViewUserComponent } from './view-user/view-user.component';
//import { ViewUserComponent } from './select-users/select-users.component';

const routes: Routes = [
  {path: '', component: NewUserComponent },
  {path: 'users/view', component: SelectUsersComponent },
  {path: 'users/view/{id}', component: ViewUserComponent },
  {path: 'users/create', component: NewUserComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
