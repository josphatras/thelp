import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { AccountService } from '../account.service';
import { Observable } from 'rxjs';
import * as $ from 'jquery';
import { Router } from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.sass']
})

export class ChangePasswordComponent implements OnInit {
	serverResponse: any
  hide: any;
	ChangePasswordForm = new FormGroup({
		user_id: new FormControl(''),
		current_password: new FormControl(''),
		new_password: new FormControl(''),
	});
  constructor(
    public dialogRef: MatDialogRef<ChangePasswordComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dataR: AccountService,
    private router: Router,
  	) { }

  ngOnInit() {
  	this.ChangePasswordForm.patchValue({
	    user_id: this.data.id
	  });
  }

  closeChangePassword(): void {
    this.dialogRef.close();
  }

	changePassword(){
    $('.changePasswordLoading').addClass('fa fa-refresh fa-spin');
    var formDetails = this.ChangePasswordForm.value;
    this.dataR.changePassword(formDetails).subscribe(
      (val) => {
        this.serverResponse = val
        if (this.serverResponse.status == 'success') {
            setTimeout(() => {
              $('.changePasswordLoading').removeClass('fa fa-refresh fa-spin');
              $('.ChangeResponse').html('<div class="alert alert-success sucess-response">'+ this.serverResponse.msg.response +'</div>');
              $('.sucess-response').fadeOut(3000);
            }, 500);
            setTimeout(() => {
              this.closeChangePassword();
              this.router.navigate(['logout']);
            }, 3500);
        }else if(this.serverResponse.status == 'fail'){
          setTimeout(() => {
            $('.changePasswordLoading').removeClass('fa fa-refresh fa-spin');
            $('.ChangeResponse').html('<div class="alert alert-danger fail-response">'+ this.serverResponse.msg.response +'</div>');
            $('.fail-response').fadeOut(3000);
          }, 500); 
        }
      }
    )
	}
}
