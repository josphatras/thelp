import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AccountService } from '../account.service';
import { EditUserComponent } from '../edit-user/edit-user.component';
import { DeleteUserComponent } from '../delete-user/delete-user.component';
import { ResetPasswordComponent } from '../reset-password/reset-password.component';
import { Observable } from 'rxjs';
import * as $ from 'jquery';

@Component({
  selector: 'app-select-users',
  templateUrl: './select-users.component.html',
  styleUrls: ['./select-users.component.sass'],
  animations: [
	trigger('detailExpand', [
	  state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
	  state('expanded', style({height: '*'})),
	  transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
	]),
  ],
})

export class SelectUsersComponent implements OnInit {
  dataSource: any;
  columnsToDisplay = ['id', 'full_name', 'role', 'location', 'phone', 'email'];
  expandedElement: any;
	serverResponse: any;
	serverData: any;
  user: any;

  delete_btn: boolean = false;
  reset_btn: boolean = false;
  constructor(private data: AccountService, private dialog: MatDialog) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem("tbl_user"));
    if (this.user.role == '2' || this.user.role == '3' || this.user.role == '9') {
      this.delete_btn = false;
      this.reset_btn = false;
    }else if(this.user.role == '1' || this.user.role == '8'){
      this.delete_btn = true;
      this.reset_btn = true;
    }

  	this.data.getUsers().subscribe(
				(val) => {
					this.serverData = val;
					this.dataSource = Object.keys(this.serverData.data).map(e=>this.serverData.data[e]);
        },
        response => {

        },
        () => {
            console.log("The POST observable is now completed.");
          }
		)
  }

popResetPassword(name,id): void {
    const dialogRef = this.dialog.open(ResetPasswordComponent,{
      width: '500px',
      data: {name : name, userId : id}
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
    });
  }
popDeleteUser(name,id): void {
  console.log(name);
    const dialogRef = this.dialog.open(DeleteUserComponent,{
      width: '500px',
      data: {name : name, userId : id}
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
    });
  }

  /*popEditUser */
  popEditUser(user): void {
    const dialogRef = this.dialog.open(EditUserComponent,{
      width: '700px',
      data: user
    });
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }
}