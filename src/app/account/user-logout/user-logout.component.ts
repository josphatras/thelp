import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-logout',
  templateUrl: './user-logout.component.html',
  styleUrls: ['./user-logout.component.sass']
})
export class UserLogoutComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
		localStorage.removeItem("tbl_user");
		localStorage.removeItem("tbl_token");
		this.router.navigate(['login']);
  }

}
