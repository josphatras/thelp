import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AccountService } from '../account.service';
import { FormGroup, FormControl } from '@angular/forms';
import * as $ from 'jquery';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.sass']
})
export class EditUserComponent implements OnInit {

	countries: Object;
	roles: Object;
	sbus: Object;
	clients: Object;
	tbl_option: Object;
	client_option: Object;
	country_input: Object;
	account_type: Object;
	serverResponse: any;

	client: boolean = false;
	sbu: boolean = false;
	accountSection: boolean = false;
	loadForm: boolean = false;
	user: any

	EditUserForm = new FormGroup({
		accountType: new FormControl(''),
		client: new FormControl(''),
		country: new FormControl(''),
		userGroup: new FormControl(''),
		sbu: new FormControl(''),
		firstName: new FormControl(''),
		middleName: new FormControl(''),
		lastName: new FormControl(''),
		email: new FormControl(''),
		phone: new FormControl(''),
		skype: new FormControl(''),
		town: new FormControl(''),
		user_id: new FormControl(''),
	});

  constructor(
  	private data: AccountService,
		public dialogRef: MatDialogRef<EditUserComponent>,
		@Inject(MAT_DIALOG_DATA) public userData: any,
		private router: Router,
  ) { }

  ngOnInit() {
		this.user = JSON.parse(localStorage.getItem("tbl_user"));
		let stringToSplit = this.userData.full_name;
		let x = stringToSplit.split(" ");
		this.EditUserForm.patchValue({
			firstName: x[0],
			middleName: x[1],
			lastName: x[2],
			// client: this.userData.client,
			country: this.userData.country,
			userGroup: this.userData.userGroup,
			sbu: this.userData.sbu,
			email: this.userData.email,
			phone: this.userData.phone,
			skype: this.userData.skype,
			town: this.userData.location,
			user_id: this.userData.id,
		});
		this.EditUserForm.controls['country'].setValue(this.userData.country_id, {onlySelf: true})
		this.EditUserForm.controls['userGroup'].setValue(this.userData.role_id, {onlySelf: true})
		this.EditUserForm.controls['sbu'].setValue(this.userData.sbu_id, {onlySelf: true})
		this.EditUserForm.controls['client'].setValue(this.userData.client_id, {onlySelf: true})

		if (this.userData.role_id == '2' || this.userData.role_id == '3' || this.userData.role_id == '9') {
			this.tbl_option = false;
			this.client_option = true;
			this.accountSection = true;
			this.clients = false;
			this.client = true;
			this.country_input = false;
			this.account_type = false;

			this.EditUserForm.controls['accountType'].setValue('client')
			this.data.getRoles('client').subscribe(
				data => {
					this.roles = data
			})
		}else{
			this.data.getRoles('tbl').subscribe(
				data => {
					this.roles = data
			})
			this.tbl_option = true;
			this.client_option = false;
			this.clients = true;
			this.client = false;
			this.country_input = true;
			this.account_type = true;

			this.EditUserForm.controls['accountType'].setValue('tbl')
		}
		this.data.getCountries().subscribe(
			data => this.countries = data
		)
		this.data.getSbus().subscribe(
			data => this.sbus = data
		)
		this.data.getClients().subscribe(
			data => this.clients = data
		)
  }

	userGroupSelection(){
		var formInputs = this.EditUserForm.value;
		var userGroup = formInputs.userGroup;
		if (userGroup == '1' || userGroup == '8' || userGroup == '2' || userGroup == '3' || userGroup == '9' ) {
			this.sbu = false;
		}else{
			this.sbu = true;
		}
	}

	saveUserChanges(){
		$('.submitLoad').addClass('fa fa-refresh fa-spin');
		var items = this.EditUserForm.value;
		var userGroup = items.userGroup;
		var completeForm = true;

		if (this.user.role == '2' || this.user.role == '3' || this.user.role == '9') {
			if (items.firstName == '' || items.lastName == '' || items.email == '') {
					completeForm = false;
				}
		}else{
			if (items.country == '' || items.userGroup == '' || items.firstName == '' || items.lastName == '' || items.email == '') {
					completeForm = false;
				}
			if (userGroup == 'tbl') {
				if (items.userGroup !== '1' || items.userGroup !== '8' ) {
					if (items.sbu == '') {
						 completeForm = false;
					}
				}      
			}else if (userGroup == 'client') {
				if (items.client) {
					 completeForm = false;
				}
			}			
		}

		if (completeForm) {
			this.data.editUser(items).subscribe(
				(val) => {
					this.serverResponse = val
					if (this.serverResponse.status == 'success') {
						$('.submitLoad').removeClass('fa fa-refresh fa-spin');
						$('.response').html('<div class="alert alert-success sucess-response">'+ this.serverResponse.msg.response +'</div>');
						$('.sucess-response').fadeOut(3000);
						setTimeout(() => {
							this.closeEditUser();
							this.router.navigate(['/users/view']);
						}, 3000);
					}else if(this.serverResponse.status == 'fail'){
						setTimeout(() => {
							$('.submitLoad').removeClass('fa fa-refresh fa-spin');
							$('.response').html('<div class="alert alert-danger fail-response">'+ this.serverResponse.msg.response +'</div>');
							$('.fail-response').fadeOut(3000);
						}, 500); 
					}
        },
        response => {
            setTimeout(() => {
							$('.submitLoad').removeClass('fa fa-refresh fa-spin');
							$('.response').html('<div class="alert alert-danger fail-response">Something went wrong</div>');
							$('.fail-response').fadeOut(3000);
						}, 500); 
        },
        () => {
            console.log("The POST observable is now completed.");
          }
			)
		}else{
			setTimeout(() => {
				$('.submitLoad').removeClass('fa fa-refresh fa-spin');
				$('.response').html('<div class="alert alert-danger fail-response"> One or more empty fields</div>');
				$('.fail-response').fadeOut(3000);
			}, 500);
		}
	}
  closeEditUser(): void {
    this.dialogRef.close();
  }
}
