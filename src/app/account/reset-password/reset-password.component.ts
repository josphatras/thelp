import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AccountService } from '../account.service';
import { Observable } from 'rxjs';
import * as $ from 'jquery';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.sass']
})
export class ResetPasswordComponent implements OnInit {
serverResponse: any
  constructor(
    public dialogRef: MatDialogRef<ResetPasswordComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dataR: AccountService,
    private router: Router, 
  ) { }

  ngOnInit() {
  }


  closeReset(): void {
    this.dialogRef.close();
  }

  resetPassword(userId){
    $('.resetPasswordLoad').addClass('fa fa-refresh fa-spin');
    this.dataR.resetPassword(userId).subscribe(
      (val) => {
        this.serverResponse = val
        if (this.serverResponse.status == 'success') {
            setTimeout(() => {
              $('.resetPasswordLoad').removeClass('fa fa-refresh fa-spin');
              $('.deleteResponse').html('<div class="alert alert-success sucess-response">'+ this.serverResponse.msg.response +'</div>');
              $('.sucess-response').fadeOut(3000);
            }, 500);
            setTimeout(() => {
              this.closeReset();
              // this.router.navigate(['/tickets/view']);
            }, 3500);
        }else if(this.serverResponse.status == 'fail'){
            setTimeout(() => {
              $('.resetPasswordLoad').removeClass('fa fa-refresh fa-spin');
              $('.resetPasswordResponse').html('<div class="alert alert-danger fail-response">'+ this.serverResponse.msg.response +'</div>');
              $('.fail-response').fadeOut(3000);
            }, 500); 
          }

      }
    )
  }
}
