import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AccountService } from '../account.service';
import { Observable } from 'rxjs';
import * as $ from 'jquery';
import { Router } from '@angular/router';


@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.sass']
})

export class UserLoginComponent implements OnInit {
	hide = true;
	serverResponse: any;
  constructor(
  	private router: Router, 
  	private data: AccountService
  	) { }
	UserLoginForm = new FormGroup({
		email: new FormControl(''),
		password: new FormControl(''),
		_token: new FormControl(''),
	});

	items: Items = new Items;

  ngOnInit() {
  	if (this.data.authKey() !== null) {
	  	this.router.navigate(['/dashboard']);
  	}
  }

  onSubmit(){
		$('.submitLoad').addClass('fa fa-refresh fa-spin');
		 this.items['username'] = this.UserLoginForm.value.email,	
		 this.items['password'] = this.UserLoginForm.value.password,
		 this.items['grant_type'] = 'password'
		 this.items['client_id'] = '2'
		 this.items['client_secret'] = 'HfN9G0ovVhGxrM5tvtmgzZOd5sFIEf1i6uolOPhO'
		 this.items['scope'] = ''
		var completeForm = true;

		if (this.items.password == '' || this.items.password == '') {
			completeForm = false;
		}

		if (completeForm) {
			//:::::::::::::::::::::::::::::::::::::::::::::::::::::;

			this.data.userLogin(this.items).subscribe(
				(val) => {
					this.serverResponse = val
					if (this.serverResponse.access_token) {
						this.tokenUser({Authorization : this.serverResponse.access_token, username : this.UserLoginForm.value.email});
					}else{
						setTimeout(() => {
							$('.submitLoad').removeClass('fa fa-refresh fa-spin');
							$('.response').html('<div class="alert alert-danger fail-response">Wrong username or password</div>');
							$('.fail-response').fadeOut(3000);
					}, 500); 
					}
				},
				response => {
					setTimeout(() => {
						$('.submitLoad').removeClass('fa fa-refresh fa-spin');
						$('.response').html('<div class="alert alert-danger fail-response">Wrong username or password</div>');
						$('.fail-response').fadeOut(3000);
					}, 500); 
				},
				() => {
					// console.log("The POST observable is now completed.");
			  }
		)
		//:::::::::::::::::::::::::::::::::::::::::::::::;
		}else{
			setTimeout(() => {
				$('.submitLoad').removeClass('fa fa-refresh fa-spin');
				$('.response').html('<div class="alert alert-danger fail-response"> One or more empty fields</div>');
				$('.fail-response').fadeOut(3000);
			}, 500);
		}
	}

	tokenUser(details){
			this.data.loggedUserDetails(details).subscribe(
				(val) => {
					this.serverResponse = val
					setTimeout(() => {
						$('.submitLoad').removeClass('fa fa-refresh fa-spin');
						$('.response').html('<div class="alert alert-success sucess-response">Successfuly logged in</div>');
						$('.sucess-response').fadeOut(3000);
						localStorage.setItem('tbl_user', JSON.stringify(this.serverResponse));
						localStorage.setItem('tbl_token',  JSON.stringify(details.Authorization));
						this.router.navigate(['/dashboard']);
					}, 500);
				},
				response => {
					setTimeout(() => {
						$('.submitLoad').removeClass('fa fa-refresh fa-spin');
						$('.response').html('<div class="alert alert-danger fail-response">Something went wrong</div>');
						$('.fail-response').fadeOut(3000);
					}, 500); 
				},
				() => {
					// console.log("The POST observable is now completed.");
			  }
		)
	}
}

export class Items{
	email : any
	password : any
	grant_type : any
	client_id : any
	client_secret : any
	username : any
	scope : any
	supportType: any
}