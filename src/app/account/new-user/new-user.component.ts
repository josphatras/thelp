import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AccountService } from '../account.service';
import { Observable } from 'rxjs';
import * as $ from 'jquery';
import { Router } from '@angular/router';

@Component({
	selector: 'app-new-user',
	templateUrl: './new-user.component.html',
	styleUrls: ['./new-user.component.sass']
})
export class NewUserComponent implements OnInit {
	countries: Object;
	roles: Object;
	sbus: Object;
	clients: Object;
	tbl_option: Object;
	client_option: Object;
	client_option_selected: Object;
	country_input: Object;
	account_type: Object;
	serverResponse: any;

	client: boolean = false;
	sbu: boolean = false;
	accountSection: boolean = false;
	loadForm: boolean = false;

	NewUserForm = new FormGroup({
		accountType: new FormControl(''),
		client: new FormControl(''),
		country: new FormControl(''),
		userGroup: new FormControl(''),
		sbu: new FormControl(''),
		firstName: new FormControl(''),
		middleName: new FormControl(''),
		lastName: new FormControl(''),
		email: new FormControl(''),
		phone: new FormControl(''),
		skype: new FormControl(''),
		town: new FormControl(''),
	});

	constructor(private router: Router, private data: AccountService) { }
	user: any
	ngOnInit() {
		this.user = JSON.parse(localStorage.getItem("tbl_user"));
		if (this.user.role == '2' || this.user.role == '3' || this.user.role == '9') {
			this.tbl_option = false;
			this.client_option = false;
			this.client_option_selected = true;
			this.accountSection = true;
			this.clients = false;
			this.country_input = false;
			this.account_type = false;

			this.data.getRoles('client').subscribe(
				data => {
					// this.roles = data
					console.log(data);
			}
			)
			this.roles = {"data":[{"id":3,"level":2,"initial":"cons","name":"Consultant","created_at":null,"updated_at":null},{"id":9,"level":2,"initial":"client_super_admin","name":"Client Super Admin","created_at":null,"updated_at":null}]};
			console.log(this.roles);
		}else{
			this.tbl_option = true;
			this.client_option = true;
			this.client_option_selected = false;
			this.clients = true;
			this.country_input = true;
			this.account_type = true;

		}
		this.data.getCountries().subscribe(
			data => this.countries = data
		)
		this.data.getSbus().subscribe(
			data => this.sbus = data
		)
		this.data.getClients().subscribe(
			data => this.clients = data
		)
	}


	updateProfile() {
		this.NewUserForm.patchValue({
			firstName: 'Nancy'
		});
	}

	accountType(){
	//console.warn(this.NewUserForm.value);
	var formInputs = this.NewUserForm.value;
	var accountType = formInputs.accountType;
	this.data.getRoles(accountType).subscribe(
		data => this.roles = data
	)
		if (accountType == 'client') {
			this.accountSection = false;
			this.sbu = false;
			this.loadForm = true;
			this.client = true;
		 setTimeout(() => {
			 this.loadForm = false;
			 this.accountSection = true;
			}, 1000);
			
		}else if(accountType == 'tbl'){
			this.accountSection = false;
			this.loadForm = true;
			this.client = false;
			setTimeout(() => {
				this.loadForm = false;
				this.accountSection = true;
			}, 500);
		}
	}

	userGroupSelection(){
		var formInputs = this.NewUserForm.value;
		var userGroup = formInputs.userGroup;
		if (userGroup == '1' || userGroup == '8' || userGroup == '2' || userGroup == '3' || userGroup == '9' ) {
			this.sbu = false;
		}else{
			this.sbu = true;
		}
	}

	onSubmit(){
		$('.submitLoad').addClass('fa fa-refresh fa-spin');
		var items = this.NewUserForm.value;
		var userGroup = items.userGroup;
		var completeForm = true;

		if (this.user.role == '2' || this.user.role == '3' || this.user.role == '9') {
			if (items.firstName == '' || items.lastName == '' || items.email == '') {
					completeForm = false;
				}
		}else{
			if (items.country == '' || items.userGroup == '' || items.firstName == '' || items.lastName == '' || items.email == '') {
					completeForm = false;
				}
			if (userGroup == 'tbl') {
				if (items.userGroup !== '1' || items.userGroup !== '8' ) {
					if (items.sbu == '') {
						 completeForm = false;
					}
				}      
			}else if (userGroup == 'client') {
				if (items.client) {
					 completeForm = false;
				}
			}			
		}

		if (completeForm) {
			this.data.saveUser(items).subscribe(
				(val) => {
					this.serverResponse = val
					if (this.serverResponse.status == 'success') {
						setTimeout(() => {
							$('.submitLoad').removeClass('fa fa-refresh fa-spin');
							$('.response').html('<div class="alert alert-success sucess-response">'+ this.serverResponse.msg.response +'</div>');
							$('.sucess-response').fadeOut(3000);
						}, 500);
						setTimeout(() => {
							this.router.navigate(['/users/view']);
						}, 3500);
					}else if(this.serverResponse.status == 'fail'){
						setTimeout(() => {
							$('.submitLoad').removeClass('fa fa-refresh fa-spin');
							$('.response').html('<div class="alert alert-danger fail-response">'+ this.serverResponse.msg.response +'</div>');
							$('.fail-response').fadeOut(3000);
						}, 500); 
					}
        },
        response => {
            setTimeout(() => {
							$('.submitLoad').removeClass('fa fa-refresh fa-spin');
							$('.response').html('<div class="alert alert-danger fail-response">Something went wrong</div>');
							$('.fail-response').fadeOut(3000);
						}, 500); 
        },
        () => {
            console.log("The POST observable is now completed.");
          }
			)
		}else{
			setTimeout(() => {
				$('.submitLoad').removeClass('fa fa-refresh fa-spin');
				$('.response').html('<div class="alert alert-danger fail-response"> One or more empty fields</div>');
				$('.fail-response').fadeOut(3000);
			}, 500);
		}
	}

}