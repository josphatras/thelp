import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewUserComponent } from './new-user.component';

/*describe('NewUserComponent', () => {
  let component: NewUserComponent;
  let fixture: ComponentFixture<NewUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
*/

/*link = 'http://127.0.0.1:8000/api/sbu/2';
getSbu(){
this.HttpClientModule.get(this.link).subscribe(
        (val) => {
            //this.offcialsData = val;
            console.log(val);
        },

        response => {
            console.log("POST call in error", response);
        }
            //console.log("The get observable is now completed.");
)};*/

describe('NewUserComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        NewUserComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(NewUserComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'front'`, () => {
    const fixture = TestBed.createComponent(NewUserComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('front');
  });

  it('should render title in a h1 tag', () => {
    const fixture = TestBed.createComponent(NewUserComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to front!');
  });
});