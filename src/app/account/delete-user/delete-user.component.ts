import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AccountService } from '../account.service';
import { Observable } from 'rxjs';
import * as $ from 'jquery';
import { Router } from '@angular/router';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.sass']
})
export class DeleteUserComponent implements OnInit {
  serverResponse: any
  constructor(
    public dialogRef: MatDialogRef<DeleteUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dataR: AccountService,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  closeDeleteUser(): void {
    this.dialogRef.close();
  }

  deleteUser(userId){
    $('.deleteLoad').addClass('fa fa-refresh fa-spin');
    this.dataR.deleteUser(userId).subscribe(
      (val) => {
        this.serverResponse = val
        if (this.serverResponse.status == 'success') {
            $('.deleteLoad').removeClass('fa fa-refresh fa-spin');
            $('.deleteResponse').html('<div class="alert alert-success sucess-response">'+ this.serverResponse.msg.response +'</div>');
            $('.sucess-response').fadeOut(3000);
            setTimeout(() => {
              this.closeDeleteUser();
              this.router.navigate(['/users/view']);
            }, 3000);
        }else if(this.serverResponse.status == 'fail'){
            setTimeout(() => {
              $('.submitLoad').removeClass('fa fa-refresh fa-spin');
              $('.response').html('<div class="alert alert-danger fail-response">'+ this.serverResponse.msg.response +'</div>');
              $('.fail-response').fadeOut(3000);
            }, 500); 
          }

      }
    )
  }
}
