import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SysConfig } from '../config/sys';


@Injectable({
  providedIn: 'root'
})
export class AccountService {
	hearderOptions: any
	generatedHeader: any
	token: any
    authKey(){
        this.token = JSON.parse(localStorage.getItem('tbl_token'))
        return this.token
    }

  	private config = SysConfig.siteUrl; 

	genHeders(token_key){
		this.generatedHeader = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json', 
				'Access-Control-Allow-Origin': '*', 
				'Authorization' : token_key,
			})
		}
		return this.generatedHeader;
	}
  constructor(
  	// private config : SysConfig, 
  	private http: HttpClient,
  ) { }

	readonly ROOT_URL = this.config + 'api/users/new';
	readonly LOGGED_DETAILS_URL = this.config+'api/logged_user';
	readonly LOGIN_URL = this.config+'oauth/token';
	readonly RESET_PASS_URL = this.config+'api/user/reset-password';
	readonly CHANGE_PASS_URL = this.config+'api/user/change-password';
	readonly DELETE_USER_URL = this.config+'api/user/delete';
	readonly EDIT_USER_URL = this.config+'api/user/edit';

  getCountries(){
		var info = this.http.get(this.config+'api/items/country', this.genHeders('Bearer ' + this.authKey()));  
		return info;
  }
	getSbus(){
		var info = this.http.get(this.config+'api/items/sbu', this.genHeders('Bearer ' + this.authKey()));  
		return info;
	}
	getRoles(type){
		if (type == 'client') {
			var info = this.http.get(this.config+'api/items/role/2', this.genHeders('Bearer ' + this.authKey())); 
		}else if(type == 'tbl'){
			var info = this.http.get(this.config+'api/items/role/1', this.genHeders('Bearer ' + this.authKey())); 
		}
		return info;
	}
	getClients(){
		var info = this.http.get(this.config+'api/client/all', this.genHeders('Bearer ' + this.authKey()));  
		return info;	
	}

	saveUser(userData){
		let url = this.ROOT_URL;
		return this.http.post(url, userData , this.genHeders('Bearer ' + this.authKey()));
	}

	getUsers(){
		var info = this.http.get(this.config+'api/users', this.genHeders('Bearer ' + this.authKey()));  
		return info;		
	}

	userLogin(logins){
		let url = this.LOGIN_URL;
		return this.http.post(url, logins);
	}

	loggedUserDetails(details){
		let url = this.LOGGED_DETAILS_URL;
		this.hearderOptions = this.genHeders('Bearer '+details.Authorization);
		return this.http.post(url, {username: details.username}, this.genHeders('Bearer ' + details.Authorization));
	}

	deleteUser(userId){
		let url = this.DELETE_USER_URL;
		return this.http.post(url, {id: userId}, this.genHeders('Bearer ' + this.authKey()));
	}

	resetPassword(userId){
		let url = this.RESET_PASS_URL;
		return this.http.post(url, {id: userId}, this.genHeders('Bearer ' + this.authKey()));
	}

	changePassword(details){
		let url = this.CHANGE_PASS_URL;
		return this.http.post(url, details, this.genHeders('Bearer ' + this.authKey()));
	}

	editUser(userData){
		let url = this.EDIT_USER_URL;
		return this.http.post(url, userData, this.genHeders('Bearer ' + this.authKey()));
	}
}
