/*import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { NewUserComponent } from './new-user/new-user.component';
//import { ViewUserComponent } from './view-user/view-user.component';
import { SelectUsersComponent } from './select-users/select-users.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';
import { ViewUserComponent } from './view-user/view-user.component';
import { AccountRoutingModule } from './account-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    BrowserModule,
    HttpModule,
    HttpClientModule,
  ],
  declarations: [NewUserComponent,SelectUsersComponent, ViewUserComponent]
})

export class AccountModule { }*/


import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AccountComponent } from './account.component';
import { RouterModule, Routes } from '@angular/router';
import { AccountRoutingModule } from './account-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { NewUserComponent } from './new-user/new-user.component';
import { SelectUsersComponent } from './select-users/select-users.component';
import { ViewUserComponent } from './view-user/view-user.component';
import { UserLoginComponent } from './user-login/user-login.component';
import { UserLogoutComponent } from './user-logout/user-logout.component';
import { DeleteUserComponent } from './delete-user/delete-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ChangePasswordComponent } from './change-password/change-password.component';


@NgModule({
  declarations: [
    AccountComponent,
    // NewUserComponent,
    // SelectUsersComponent,
    // ViewUserComponent,
    // UserLoginComponent,
    // UserLogoutComponent,
    // DeleteUserComponent,
    // EditUserComponent,
    // ResetPasswordComponent,
    // ChangePasswordComponent
  ],
  imports: [
    BrowserModule,
    AccountRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
  AccountRoutingModule],
  bootstrap: [AccountComponent]
})

export class AccountModule { }
