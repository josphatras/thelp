import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientRoutingModule } from './client-routing.module';
import { NewClientComponent } from './new-client/new-client.component';
import { ViewClientComponent } from './view-client/view-client.component';
// import { EditClientComponent } from './edit-client/edit-client.component';
// import { DeleteClientComponent } from './delete-client/delete-client.component';

@NgModule({
  imports: [
    CommonModule,
    ClientRoutingModule
  ],
  declarations: [
  	// NewClientComponent, ViewClientComponent
  // EditClientComponent,
  	// DeleteClientComponent,
  ]
})
export class ClientModule { }
