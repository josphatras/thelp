import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ClientService } from '../client.service';
import { EditClientComponent } from '../edit-client/edit-client.component';
import { DeleteClientComponent } from '../delete-client/delete-client.component';
import { Observable } from 'rxjs';
import * as $ from 'jquery';

@Component({
	selector: 'app-view-client',
	templateUrl: './view-client.component.html',
	styleUrls: ['./view-client.component.sass'],
		animations: [
	trigger('detailExpand', [
		state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
		state('expanded', style({height: '*'})),
		transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
	]),
	],
})

export class ViewClientComponent implements OnInit {
	dataSource: any;
	columnsToDisplay = ['name', 'email', 'phone', 'country', 'status', 'created_at'];
	expandedElement: any;
	serverResponse: any;
	serverData: any;
	projectOption: any;
	user: any;

	edit_btn: boolean = false;
	delete_btn: boolean = false;
	constructor(
		private data: ClientService, 
		private _Activatedroute:ActivatedRoute,
		private route: ActivatedRoute,
		public dialog: MatDialog,
		) { }

	ngOnInit() {
		this.user = JSON.parse(localStorage.getItem("tbl_user"));

		if (this.user.role == '2' || this.user.role == '3' || this.user.role == '9') {
			this.edit_btn = false;
			this.delete_btn = false;
		}else if(this.user.role == '1' || this.user.role == '8'){
			this.edit_btn = true;
			this.delete_btn = true;
		}

		this.data.viewClients().subscribe(
				(val) => {
					this.serverData = val;
					this.dataSource = Object.keys(this.serverData).map(e=>this.serverData[e]);
				},
				response => {

				},
				() => {
						// console.log("The observable is now completed.");
					}
		)
	}
	popEdit(client): void {
		const dialogRef = this.dialog.open(EditClientComponent,{
			width: '500px',
			data: {client: client}});

		dialogRef.afterClosed().subscribe(result => {
			// console.log('The dialog was closed');
			this.ngOnInit();
		});
	}
	popDelete(client): void {
		const dialogRef = this.dialog.open(DeleteClientComponent,{
			width: '500px',
			data: {client_id: client.id, client_name: client.name}
		});

		dialogRef.afterClosed().subscribe(result => {
			// console.log('The dialog was closed');
		});
	}
}
