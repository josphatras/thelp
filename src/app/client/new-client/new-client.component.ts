import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ClientService } from '../client.service';
import { Observable } from 'rxjs';
import * as $ from 'jquery';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-client',
  templateUrl: './new-client.component.html',
  styleUrls: ['./new-client.component.sass']
})
export class NewClientComponent implements OnInit {

	// countries: Object;
	countries: any;
	serverResponse: any;
	projectManagers: any;

	name: Boolean = false;
	email: Boolean = false;
	phone: Boolean = false;

	NewClientForm = new FormGroup({
		country: new FormControl(''),
		name: new FormControl(''),
		email: new FormControl(''),
		phone: new FormControl(''),
	});

  constructor(private router: Router, private data: ClientService) { }

  ngOnInit() {
		this.data.getCountries().subscribe(
			data => {this.countries = data}
		)
  }

    onSubmit(){
		$('.submitLoad').addClass('fa fa-refresh fa-spin');
		var items = this.NewClientForm.value;
		var supportType = items.supportType;
		var completeForm = true;

		if (items.country == '' || items.name == '' || items.email == '' || items.phone == '') {
				completeForm = false;
			}

		if (completeForm) {
			this.data.saveClient(items).subscribe(
				(val) => {
					this.serverResponse = val
					if (this.serverResponse.status == 'success') {
						setTimeout(() => {
							$('.submitLoad').removeClass('fa fa-refresh fa-spin');
							$('.response').html('<div class="alert alert-success sucess-response">'+ this.serverResponse.msg.response +'</div>');
							$('.sucess-response').fadeOut(3000);
						}, 500);
						setTimeout(() => {
							this.router.navigate(['/clients/view']);
						}, 3500);
					}else if(this.serverResponse.status == 'fail'){
						setTimeout(() => {
							$('.submitLoad').removeClass('fa fa-refresh fa-spin');
							$('.response').html('<div class="alert alert-danger fail-response">'+ this.serverResponse.msg.response +'</div>');
							$('.fail-response').fadeOut(3000);
						}, 500); 
					}
        },
        response => {
            setTimeout(() => {
							$('.submitLoad').removeClass('fa fa-refresh fa-spin');
							$('.response').html('<div class="alert alert-danger fail-response">Something went wrong</div>');
							$('.fail-response').fadeOut(3000);
						}, 500); 
        },
        () => {
            console.log("The POST observable is now completed.");
          }
			)
		}else{
			setTimeout(() => {
				$('.submitLoad').removeClass('fa fa-refresh fa-spin');
				$('.response').html('<div class="alert alert-danger fail-response"> One or more empty fields</div>');
				$('.fail-response').fadeOut(3000);
			}, 500);
		}
	}
}
