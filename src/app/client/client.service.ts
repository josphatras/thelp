import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SysConfig } from '../config/sys';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
    hearderOptions: any
    generatedHeader: any
    token: any
    authKey(){
        this.token = JSON.parse(localStorage.getItem('tbl_token'))
        return this.token
    }

    genHeders(token_key){
        this.generatedHeader = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json', 
                'Access-Control-Allow-Origin': '*', 
                'Authorization' : token_key,
            })
        }
        return this.generatedHeader;
    }

  private config = SysConfig.siteUrl; 
  readonly CREATE_URL = this.config+'api/clients/new';
  readonly DELETE_CLIENT_URL = this.config+'api/client/delete';
	readonly EDIT_URL = this.config+'api/client/edit';
  constructor(private http: HttpClient) { }

	getCountries(){
		var info = this.http.get(this.config+'api/items/country',this.genHeders('Bearer ' + this.authKey()));  
		return info;
  }

  saveClient(clientData){
  	let url = this.CREATE_URL;
		return this.http.post(url, clientData,this.genHeders('Bearer ' + this.authKey()));
  }

  viewClients(){
  	var info = this.http.get(this.config+'api/clients/show', this.genHeders('Bearer ' + this.authKey()));  
  	return info; 	
  }

  deleteClient(client_id){
     let url = this.DELETE_CLIENT_URL;
    return this.http.post(url, {'id': client_id} , this.genHeders('Bearer ' + this.authKey()));    
  }

  editClient(clientData){
    let url = this.EDIT_URL;
    return this.http.post(url, clientData,this.genHeders('Bearer ' + this.authKey()));    
  }
}
