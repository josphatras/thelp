import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ClientService } from '../client.service';
import { Observable } from 'rxjs';
import * as $ from 'jquery';
import { Router } from '@angular/router';

@Component({
  selector: 'app-delete-client',
  templateUrl: './delete-client.component.html',
  styleUrls: ['./delete-client.component.sass']
})
export class DeleteClientComponent implements OnInit {
	serverResponse: any;
  constructor(
    public dialogRef: MatDialogRef<DeleteClientComponent>,
    @Inject(MAT_DIALOG_DATA) public dataI: any,
    private data: ClientService,
    private router: Router, 
  	) { }

  ngOnInit() {
  	
  }
  closeDelete(): void {
    this.dialogRef.close();
  }

  deleteClient(clientId){
    $('.deleteLoad').addClass('fa fa-refresh fa-spin');
    this.data.deleteClient(clientId).subscribe(
      (val) => {
        this.serverResponse = val
        if (this.serverResponse.status == 'success') {
            setTimeout(() => {
              $('.deleteLoad').removeClass('fa fa-refresh fa-spin');
              $('.deleteResponse').html('<div class="alert alert-success sucess-response">'+ this.serverResponse.msg.response +'</div>');
              $('.sucess-response').fadeOut(3000);
            }, 500);
            setTimeout(() => {
              this.closeDelete();
              this.router.navigate(['/clients/view']);
            }, 3500);
        }else if(this.serverResponse.status == 'fail'){
            setTimeout(() => {
              $('.submitLoad').removeClass('fa fa-refresh fa-spin');
              $('.response').html('<div class="alert alert-danger fail-response">'+ this.serverResponse.msg.response +'</div>');
              $('.fail-response').fadeOut(3000);
            }, 500); 
          }

      }
    )
  }
}
