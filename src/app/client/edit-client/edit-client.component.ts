import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { ClientService } from '../client.service';
import { Observable } from 'rxjs';
import * as $ from 'jquery';
import { Router } from '@angular/router';

@Component({
	selector: 'app-edit-client',
	templateUrl: './edit-client.component.html',
	styleUrls: ['./edit-client.component.sass']
})
export class EditClientComponent implements OnInit {
	countries: any;
	serverResponse: any;
	projectManagers: any;

	name: Boolean = false;
	email: Boolean = false;
	phone: Boolean = false;


	EditClientForm = new FormGroup({
		country: new FormControl(''),
		name: new FormControl(''),
		email: new FormControl(''),
		phone: new FormControl(''),
		id: new FormControl(''),
	});
	constructor(
		public dialogRef: MatDialogRef<EditClientComponent>,
		@Inject(MAT_DIALOG_DATA) public dataI: any,
		private data: ClientService,
		private router: Router, 
		) { }

	ngOnInit() {
		this.EditClientForm.patchValue({
			country: this.dataI.country,
			name: this.dataI.client.name,
			email: this.dataI.client.email,
			phone: this.dataI.client.phone,
			id: this.dataI.client.id,
		});
		this.EditClientForm.controls['country'].setValue('5', {onlySelf: true})
		this.data.getCountries().subscribe(
			data => {
				this.countries = data
			}
		)
	}

	closeEdit(): void {
		this.dialogRef.close();
	}
    onSubmit(){
		$('.submitLoad').addClass('fa fa-refresh fa-spin');
		var items = this.EditClientForm.value;
		var supportType = items.supportType;
		var completeForm = true;

		if (items.country == '' || items.name == '' || items.email == '' || items.phone == '') {
				completeForm = false;
			}

		if (completeForm) {
			this.data.editClient(items).subscribe(
				(val) => {
					this.serverResponse = val
					if (this.serverResponse.status == 'success') {
						setTimeout(() => {
							$('.submitLoad').removeClass('fa fa-refresh fa-spin');
							$('.response').html('<div class="alert alert-success sucess-response">'+ this.serverResponse.msg.response +'</div>');
							$('.sucess-response').fadeOut(3000);
						}, 500);
						setTimeout(() => {
							this.closeEdit();
							this.router.navigate(['/clients/view']);
						}, 3500);
					}else if(this.serverResponse.status == 'fail'){
						setTimeout(() => {
							$('.submitLoad').removeClass('fa fa-refresh fa-spin');
							$('.response').html('<div class="alert alert-danger fail-response">'+ this.serverResponse.msg.response +'</div>');
							$('.fail-response').fadeOut(3000);
						}, 500); 
					}
        },
        response => {
            setTimeout(() => {
							$('.submitLoad').removeClass('fa fa-refresh fa-spin');
							$('.response').html('<div class="alert alert-danger fail-response">Something went wrong</div>');
							$('.fail-response').fadeOut(3000);
						}, 500); 
        },
        () => {
            console.log("The POST observable is now completed.");
          }
			)
		}else{
			setTimeout(() => {
				$('.submitLoad').removeClass('fa fa-refresh fa-spin');
				$('.response').html('<div class="alert alert-danger fail-response"> One or more empty fields</div>');
				$('.fail-response').fadeOut(3000);
			}, 500);
		}
	}	
}
