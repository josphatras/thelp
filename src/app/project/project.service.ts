import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SysConfig } from '../config/sys';

const hearderOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'})
}


@Injectable({
  providedIn: 'root'
})

export class ProjectService {
    hearderOptions: any
    generatedHeader: any
    token: any
    authKey(){
        this.token = JSON.parse(localStorage.getItem('tbl_token'))
        return this.token
    }

    genHeders(token_key){
        this.generatedHeader = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json', 
                'Access-Control-Allow-Origin': '*', 
                'Authorization' : token_key,
            })
        }
        return this.generatedHeader;
    }

    private config = SysConfig.siteUrl; 

  readonly CREATE_URL = this.config+'api/projects/new';
  readonly DELETE_URL = this.config+'api/project/delete';
  readonly Edit_URL = this.config+'api/project/edit';
  constructor(private http: HttpClient) { }

  getCountries(){
        var info = this.http.get(this.config+'api/items/country', this.genHeders('Bearer ' + this.authKey()));  
        return info;
  }
    getSbus(){
        var info = this.http.get(this.config+'api/items/sbu', this.genHeders('Bearer ' + this.authKey()));  
        return info;
    }
    getClients(){
        var info = this.http.get(this.config+'api/client/all', this.genHeders('Bearer ' + this.authKey()));  
        return info;    
    }

  getProjects(){
        var info = this.http.get(this.config+'api/items/country', this.genHeders('Bearer ' + this.authKey()));  
        return info;
  }

  getPms(sbuId){
    var info = this.http.get(this.config+'api/users/sbu-pms/' + sbuId, this.genHeders('Bearer ' + this.authKey()));  
    return info;  
  }

  viewProjects(option){
    var info = this.http.get(this.config+'api/projects/show/'+ option, this.genHeders('Bearer ' + this.authKey()));  
    return info;
  }

  saveProject(projectData){
    let url = this.CREATE_URL;
    return this.http.post(url, projectData, this.genHeders('Bearer ' + this.authKey()));
  }
  deleteProject(project_id){
    let url = this.DELETE_URL;
    return this.http.post(url, {'id' : project_id}, this.genHeders('Bearer ' + this.authKey()));
  }
  editProject(projectData){
    let url = this.Edit_URL;
    return this.http.post(url, projectData, this.genHeders('Bearer ' + this.authKey()));
  }
}

