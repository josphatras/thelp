import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { ProjectService } from '../project.service';
import { Observable } from 'rxjs';
import * as $ from 'jquery';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-project',
  templateUrl: './edit-project.component.html',
  styleUrls: ['./edit-project.component.sass']
})
export class EditProjectComponent implements OnInit {
	// countries: Object;
	// sbus: Object;
	// clients: Object;
	countries: any;
	sbus: any;
	clients: any;
	serverResponse: any;
	projectManagers: any;

	supportHours: Boolean = false;
	billingCycle: Boolean = false;
	supportRate: Boolean = false;

	EditProjectForm = new FormGroup({
		project: new FormControl(''),
		country: new FormControl(''),
		client: new FormControl(''),
		supportType: new FormControl(''),
		supportHours: new FormControl(''),
		billingCycle: new FormControl(''),
		supportRate: new FormControl(''),
		contract_start: new FormControl(''),
		contract_end: new FormControl(''),
		sbu: new FormControl(''),
		projectManager: new FormControl(''),
		sapCode: new FormControl(''),
	});

  constructor(
  	private router: Router, 
  	private data: ProjectService,
		public dialogRef: MatDialogRef<EditProjectComponent>,
		@Inject(MAT_DIALOG_DATA) public dataI: any,
  ) { }

  ngOnInit() {
		this.EditProjectForm.patchValue({
			project: this.dataI.project.project_name,
			country: this.dataI.project.country,
			client: this.dataI.project.client,
			supportType: this.dataI.project.support,
			supportHours: this.dataI.project.fixed_hours,
			billingCycle: this.dataI.project.material_cycle,
			supportRate: this.dataI.project.material_hours,
			contract_start: this.dataI.project.contract_start,
			contract_end: this.dataI.project.contract_end,
			// sbu: this.dataI.project.,
			projectManager: this.dataI.project.manager,
			sapCode: this.dataI.project.code,
		});
		if (this.dataI.project.support_id == '1') {
			this.supportHours = true;
			this.billingCycle = false;
			this.supportRate = false;
		}else if (this.dataI.project.support_id == '2') {
			this.supportHours = false;
			this.billingCycle = true;
			this.supportRate = true;
		}


		this.data.getClients().subscribe(
			data => {
				this.clients = data
			}
		)
		this.data.getCountries().subscribe(
			data => {
				this.countries = data
				console.log(data);
			}
		)
			this.data.getSbus().subscribe(
			data => {
				this.sbus = data
			}
		)
  }
	ngAfterContentInit(){
		this.EditProjectForm.controls['country'].setValue(parseInt(this.dataI.project.country_id), {onlySelf: true})
		this.EditProjectForm.controls['supportType'].setValue(this.dataI.project.support_id, {onlySelf: true})
		this.EditProjectForm.controls['client'].setValue(this.dataI.project.client_id, {onlySelf: true})
	}
  supportType(){
  	var formInputs = this.EditProjectForm.value;
		var supportType = formInputs.supportType;
		if (supportType == '1') {
			this.supportHours = true;
			this.billingCycle = false;
			this.supportRate = false;
		}else if (supportType == '2') {
			this.supportHours = false;
			this.billingCycle = true;
			this.supportRate = true;
		}
  }
  closeDelete(): void {
    this.dialogRef.close();
  }
  onSubmit(){
		$('.submitLoad').addClass('fa fa-refresh fa-spin');
		var items = this.EditProjectForm.value;
		var supportType = items.supportType;
		var completeForm = true;

		if (items.supportType == '' || items.project == '' || items.country == '' || items.client == '' || items.sbu == '' || items.projectManager == '') {
				completeForm = false;
			}
		if (supportType == '1') {
			if (items.supportHours == '') {
				 completeForm = false;
			}     
		}else if (supportType == '2') {
			if (items.billingCycle == '' || items.supportRate == '') {
				 completeForm = false;
			}
		}

		if (completeForm) {
			this.data.editProject(items).subscribe(
				(val) => {
					this.serverResponse = val
					if (this.serverResponse.status == 'success') {
						setTimeout(() => {
							$('.submitLoad').removeClass('fa fa-refresh fa-spin');
							$('.edit-response').html('<div class="alert alert-success sucess-response">'+ this.serverResponse.msg.response +'</div>');
							$('.sucess-response').fadeOut(3000);
						}, 500);
						setTimeout(() => {
              this.closeDelete();
							this.router.navigate(['/projects/show/active']);
						}, 3500);
					}else if(this.serverResponse.status == 'fail'){
						setTimeout(() => {
							$('.submitLoad').removeClass('fa fa-refresh fa-spin');
							$('.edit-response').html('<div class="alert alert-danger fail-response">'+ this.serverResponse.msg.response +'</div>');
							$('.fail-response').fadeOut(3000);
						}, 500); 
					}
        },
        response => {
            setTimeout(() => {
							$('.submitLoad').removeClass('fa fa-refresh fa-spin');
							$('.edit-response').html('<div class="alert alert-danger fail-response">Something went wrong</div>');
							$('.fail-response').fadeOut(3000);
						}, 500); 
        },
        () => {
            console.log("The POST observable is now completed.");
          }
			)
		}else{
			setTimeout(() => {
				$('.submitLoad').removeClass('fa fa-refresh fa-spin');
				$('.edit-response').html('<div class="alert alert-danger fail-response"> One or more empty fields</div>');
				$('.fail-response').fadeOut(3000);
			}, 500);
		}
	}

	getSbuPms(){
		var items = this.EditProjectForm.value;
		this.data.getPms(items.sbu).subscribe(
			data => {
				this.projectManagers = data
			}
		)
	}
}
