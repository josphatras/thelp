import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EditProjectComponent } from '../edit-project/edit-project.component';
import { DeleteProjectComponent } from '../delete-project/delete-project.component';
import { ProjectService } from '../project.service';
import { Observable } from 'rxjs';
import * as $ from 'jquery';

@Component({
  selector: 'app-view-projects',
  templateUrl: './view-projects.component.html',
  styleUrls: ['./view-projects.component.sass'],
	animations: [
	trigger('detailExpand', [
	  state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
	  state('expanded', style({height: '*'})),
	  transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
	]),
  ],
})
export class ViewProjectsComponent implements OnInit {
	dataSource: any;
	columnsToDisplay = ['project_name', 'client', 'code', 'sbu', 'support', 'contract_end', 'contract_start', 'country', 'manager'];
	expandedElement: any;
	serverResponse: any;
	serverData: any;
	projectOption: any;
	user: any;

	edit_btn: boolean = false;
	delete_btn: boolean = false;
  constructor(
  	private data: ProjectService, 
  	private _Activatedroute:ActivatedRoute,
		private route: ActivatedRoute,
		public dialog: MatDialog,
  	) { }

  ngOnInit() {
		this.user = JSON.parse(localStorage.getItem("tbl_user"));

		if (this.user.role == '2' || this.user.role == '3' || this.user.role == '9') {
			this.edit_btn = false;
			this.delete_btn = false;
		}else if(this.user.role == '1' || this.user.role == '8'){
			this.edit_btn = true;
			this.delete_btn = true;
		}

  	this.data.viewProjects(this._Activatedroute.snapshot.params['option']).subscribe(
				(val) => {
					this.serverData = val;
					this.dataSource = Object.keys(this.serverData).map(e=>this.serverData[e]);
					// console.log(val);
        },
        response => {

        },
        () => {
            // console.log("The observable is now completed.");
          }
		)
  }
	popEdit(project): void {
		console.log(project);
		const dialogRef = this.dialog.open(EditProjectComponent,{
			width: '800px',
			data: {project: project}});

		dialogRef.afterClosed().subscribe(result => {
			// console.log('The dialog was closed');
			this.ngOnInit();
		});
	}
	popDelete(project): void {
		const dialogRef = this.dialog.open(DeleteProjectComponent,{
			width: '500px',
			data: {project_id: project.id, project_name: project.project_name}
		});

		dialogRef.afterClosed().subscribe(result => {
			// console.log('The dialog was closed');
		});
	}
}
