import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ProjectService } from '../project.service';
import { Observable } from 'rxjs';
import * as $ from 'jquery';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.sass']
})
export class NewProjectComponent implements OnInit {
	// countries: Object;
	// sbus: Object;
	// clients: Object;
	countries: any;
	sbus: any;
	clients: any;
	serverResponse: any;
	projectManagers: any;

	supportHours: Boolean = false;
	billingCycle: Boolean = false;
	supportRate: Boolean = false;

	NewProjectForm = new FormGroup({
		project: new FormControl(''),
		country: new FormControl(''),
		client: new FormControl(''),
		supportType: new FormControl(''),
		supportHours: new FormControl(''),
		billingCycle: new FormControl(''),
		supportRate: new FormControl(''),
		contract_start: new FormControl(''),
		contract_end: new FormControl(''),
		sbu: new FormControl(''),
		projectManager: new FormControl(''),
		sapCode: new FormControl(''),
	});

  constructor(private router: Router, private data: ProjectService) { }

  ngOnInit() {
		this.data.getCountries().subscribe(
			data => {this.countries = data}
		)
		this.data.getClients().subscribe(
			data => {
				this.clients = data
			}
		)
			this.data.getSbus().subscribe(
			data => {
				this.sbus = data
			}
		)
  }

  supportType(){
  	var formInputs = this.NewProjectForm.value;
		var supportType = formInputs.supportType;
		if (supportType == '1') {
			this.supportHours = true;
			this.billingCycle = false;
			this.supportRate = false;
		}else if (supportType == '2') {
			this.supportHours = false;
			this.billingCycle = true;
			this.supportRate = true;
		}
  }

  onSubmit(){
		$('.submitLoad').addClass('fa fa-refresh fa-spin');
		var items = this.NewProjectForm.value;
		var supportType = items.supportType;
		var completeForm = true;

		if (items.supportType == '' || items.project == '' || items.country == '' || items.client == '' || items.sbu == '' || items.projectManager == '') {
				completeForm = false;
			}
		if (supportType == '1') {
			if (items.supportHours == '') {
				 completeForm = false;
			}     
		}else if (supportType == '2') {
			if (items.billingCycle == '' || items.supportRate == '') {
				 completeForm = false;
			}
		}

		if (completeForm) {
			this.data.saveProject(items).subscribe(
				(val) => {
					this.serverResponse = val
					if (this.serverResponse.status == 'success') {
						setTimeout(() => {
							$('.submitLoad').removeClass('fa fa-refresh fa-spin');
							$('.response').html('<div class="alert alert-success sucess-response">'+ this.serverResponse.msg.response +'</div>');
							$('.sucess-response').fadeOut(3000);
						}, 500);
						setTimeout(() => {
							this.router.navigate(['/projects/show/active']);
						}, 3500);
					}else if(this.serverResponse.status == 'fail'){
						setTimeout(() => {
							$('.submitLoad').removeClass('fa fa-refresh fa-spin');
							$('.response').html('<div class="alert alert-danger fail-response">'+ this.serverResponse.msg.response +'</div>');
							$('.fail-response').fadeOut(3000);
						}, 500); 
					}
        },
        response => {
            setTimeout(() => {
							$('.submitLoad').removeClass('fa fa-refresh fa-spin');
							$('.response').html('<div class="alert alert-danger fail-response">Something went wrong</div>');
							$('.fail-response').fadeOut(3000);
						}, 500); 
        },
        () => {
            console.log("The POST observable is now completed.");
          }
			)
		}else{
			setTimeout(() => {
				$('.submitLoad').removeClass('fa fa-refresh fa-spin');
				$('.response').html('<div class="alert alert-danger fail-response"> One or more empty fields</div>');
				$('.fail-response').fadeOut(3000);
			}, 500);
		}
	}

	getSbuPms(){
		var items = this.NewProjectForm.value;
		this.data.getPms(items.sbu).subscribe(
			data => {
				this.projectManagers = data
			}
		)
	}
}