import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectRoutingModule } from './project-routing.module';
import { NewProjectComponent } from './new-project/new-project.component';
import { ViewProjectsComponent } from './view-projects/view-projects.component';
import { DeleteProjectComponent } from './delete-project/delete-project.component';
import { EditProjectComponent } from './edit-project/edit-project.component';

@NgModule({
  imports: [
    CommonModule,
    ProjectRoutingModule
  ],
  declarations: [
  	// NewProjectComponent, ViewProjectsComponent, DeleteProjectComponent, EditProjectComponent
  ]
})
export class ProjectModule { }
