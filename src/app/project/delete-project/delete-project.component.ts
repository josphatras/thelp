import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ProjectService } from '../project.service';
import { Observable } from 'rxjs';
import * as $ from 'jquery';
import { Router } from '@angular/router';

@Component({
  selector: 'app-delete-project',
  templateUrl: './delete-project.component.html',
  styleUrls: ['./delete-project.component.sass']
})
export class DeleteProjectComponent implements OnInit {
	serverResponse: any;
  constructor(
    public dialogRef: MatDialogRef<DeleteProjectComponent>,
    @Inject(MAT_DIALOG_DATA) public dataI: any,
    private data: ProjectService,
    private router: Router, 
  	) { }

  ngOnInit() {
  	
  }
  closeDelete(): void {
    this.dialogRef.close();
  }

  deleteProject(projectId){
    $('.deleteLoad').addClass('fa fa-refresh fa-spin');
    this.data.deleteProject(projectId).subscribe(
      (val) => {
        this.serverResponse = val
        if (this.serverResponse.status == 'success') {
            setTimeout(() => {
              $('.deleteLoad').removeClass('fa fa-refresh fa-spin');
              $('.deleteResponse').html('<div class="alert alert-success sucess-response">'+ this.serverResponse.msg.response +'</div>');
              $('.sucess-response').fadeOut(3000);
            }, 500);
            setTimeout(() => {
              this.closeDelete();
              this.router.navigate(['/projects/show/active']);
            }, 3500);
        }else if(this.serverResponse.status == 'fail'){
            setTimeout(() => {
              $('.submitLoad').removeClass('fa fa-refresh fa-spin');
              $('.response').html('<div class="alert alert-danger fail-response">'+ this.serverResponse.msg.response +'</div>');
              $('.fail-response').fadeOut(3000);
            }, 500); 
          }

      }
    )
  }
}
