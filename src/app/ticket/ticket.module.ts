import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatPaginatorModule } from '@angular/material/paginator';

import { TicketRoutingModule } from './ticket-routing.module';
import { NewTicketComponent } from './new-ticket/new-ticket.component';
import { ViewTicketsComponent } from './view-tickets/view-tickets.component';
import { ViewTicketComponent } from './view-ticket/view-ticket.component';
import { EditTicketComponent } from './edit-ticket/edit-ticket.component';
import { EscalateTicketComponent } from './escalate-ticket/escalate-ticket.component';
import { AssignTicketComponent } from './assign-ticket/assign-ticket.component';
import { DeleteTicketComponent } from './delete-ticket/delete-ticket.component';
import { JobCardComponent } from './job-card/job-card.component';
import { MonthlyReportComponent } from './monthly-report/monthly-report.component';
import { ServiceTrackerReportComponent } from './service-tracker-report/service-tracker-report.component';

@NgModule({
  imports: [
    CommonModule,
    TicketRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatPaginatorModule
  ],
  declarations: [
  	// NewTicketComponent, ViewTicketsComponent, ViewTicketComponent, EditTicketComponent, EscalateTicketComponent, AssignTicketComponent, DeleteTicketComponent,
  	// JobCardComponent,
  	// MonthlyReportComponent,
  	// ServiceTrackerReportComponent
    ]
})
export class TicketModule { }
