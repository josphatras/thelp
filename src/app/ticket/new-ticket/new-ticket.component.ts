import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { TicketService } from '../ticket.service';
import { Observable } from 'rxjs';
import * as $ from 'jquery';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-ticket',
  templateUrl: './new-ticket.component.html',
  styleUrls: ['./new-ticket.component.sass']
})
export class NewTicketComponent implements OnInit {
	// projects: Object;
	// priorities: Object;
	// issues: Object;
  projects: any;
  priorities: any;
  issues: any;
	serverResponse: any;
  input: any;

	CreateTicketForm = new FormGroup({
		project: new FormControl(''),
		priority: new FormControl(''),
		issue: new FormControl(''),
    details: new FormControl(''),
		file: new FormControl(''),
	});

  form: FormGroup;
  @ViewChild('fileInput') fileInput: ElementRef;
  msgSuccess: any;
  res: any;
  noFile: boolean = true;
  hideMsgsuccess: boolean = false;
  sending: boolean = false;
  hideMsg: boolean = false;
  uploadingform: boolean = false;
  msg: any;
  formFiles: any;

  constructor(    
  	private fb: FormBuilder,
  	private router: Router, 
  	private data: TicketService,
  ) { this.createForm(); }

  ngOnInit() {
		this.data.getProjects().subscribe(
			data => {this.projects = data}
		)
		this.data.getPriorities().subscribe(
			data => {this.priorities = data}
		)
		this.data.getIssues().subscribe(
			data => {this.issues = data}
		)
  }

  createForm(){
    this.CreateTicketForm = this.fb.group({
      file: null,
      project: null,
      priority: null,
      issue: null,
      details: null,
    });
  }
  onFileChange(event) {
    if (event.target.files.length > 0) {
      let file = event.target.files[0];
      this.CreateTicketForm.get('file').setValue(file);
    }
    if(event.target.files.length < 1){
      this.noFile = true;
    }else{
      this.noFile = false;
    }
  }
  private prepareSave(): any {
    let input = new FormData();
    input.append('file', this.CreateTicketForm.get('file').value);
    input.append('project', this.CreateTicketForm.get('project').value);
    input.append('priority', this.CreateTicketForm.get('priority').value);
    input.append('issue', this.CreateTicketForm.get('issue').value);
    input.append('details', this.CreateTicketForm.get('details').value);
    console.log(input);
    this.noFile = false;
    return input;
  }
  clearFile() {
    this.CreateTicketForm.get('file').setValue(null);
    this.fileInput.nativeElement.value = '';
    this.noFile = true;
  }
  onSubmitFile() {
    this.sending = true;
    this.uploadingform = true;
    const formModel = this.prepareSave();
    this.data.createTicket(formModel).subscribe(data=>{

      // this.hideMsgsuccess = true;
      // this.res = data
      // this.sending = false;
      // this.uploadingform = false;
      // this.msgSuccess = 'Upload Successful'
    });
    setTimeout(() => {
      this.clearFile();
    }, 500);
  }

  check(){
    console.log(this.CreateTicketForm.get('file').value)
    // var flle = document.getElementById('file');
    // console.log(file);
  }
  readFile(file: File) {
    var reader = new FileReader();
    reader.onload = () => {
        console.log(reader.result);
        this.input.append('files', reader.result);
    };
    reader.readAsText(file);
  }

  file:any;
  fileChanged(e) {
      if (e.target.files.length >= 1) {
        var files = e.target.files;
        for (let b = 0; b < files.length; ++b) {
          this.file = e.target.files[b];
          let fileReader = new FileReader();
          fileReader.onload = (e) => {
            // this.formFiles = fileReader.result
            this.CreateTicketForm.patchValue({
              file: fileReader.result
            });
          }
          fileReader.readAsBinaryString(this.file);
        }
      }

  }

  onSubmit(){
    $('.submitLoad').addClass('fa fa-refresh fa-spin');
    var completeForm = true;
    if (this.CreateTicketForm.get('project').value == '' || this.CreateTicketForm.get('priority').value == '' || this.CreateTicketForm.get('issue').value == '' || this.CreateTicketForm.get('details').value == '') {
      completeForm = false;
    }
    if (completeForm) {
      this.input = new FormData();
      this.input.append('file', this.CreateTicketForm.get('file').value);
      this.input.append('project', this.CreateTicketForm.get('project').value);
      this.input.append('priority', this.CreateTicketForm.get('priority').value);
      this.input.append('issue', this.CreateTicketForm.get('issue').value);
      this.input.append('details', this.CreateTicketForm.get('details').value);
      
/*      if ($('input[type=file]')[0].files.length >= 1) {
        var files = $('input[type=file]')[0].files;
        for (let b = 0; b < files.length; ++b) {
          input.append('files', files[b]);
        }
      }*/
      console.log(this.formFiles);

      var convertedJSON = {};
      this.input.forEach(function(value, key) { 
          convertedJSON[key] = value;
      });

      this.data.createTicket(convertedJSON).subscribe(
				(val) => {
					this.serverResponse = val
					if (this.serverResponse.status == 'success') {
						setTimeout(() => {
							$('.submitLoad').removeClass('fa fa-refresh fa-spin');
							$('.response').html('<div class="alert alert-success sucess-response">'+ this.serverResponse.msg.response +'</div>');
							$('.sucess-response').fadeOut(3000);
						}, 500);
						setTimeout(() => {
							this.router.navigate(['/ticket/'+ this.serverResponse.msg.ticket_id]);
						}, 500);

					}else if(this.serverResponse.status == 'fail'){
						setTimeout(() => {
							$('.submitLoad').removeClass('fa fa-refresh fa-spin');
							$('.response').html('<div class="alert alert-danger fail-response">'+ this.serverResponse.msg.response +'</div>');
							$('.fail-response').fadeOut(3000);
						}, 500); 
					}
        },
        response => {
            setTimeout(() => {
							$('.submitLoad').removeClass('fa fa-refresh fa-spin');
							$('.response').html('<div class="alert alert-danger fail-response">Something went wrong</div>');
							$('.fail-response').fadeOut(3000);
						}, 500); 
        },
        () => {
            //console.log("The POST observable is now completed.");
          }
			)
		}else{
			setTimeout(() => {
				$('.submitLoad').removeClass('fa fa-refresh fa-spin');
				$('.response').html('<div class="alert alert-danger fail-response"> One or more empty fields</div>');
				$('.fail-response').fadeOut(3000);
			}, 500);
		}
	}
}
