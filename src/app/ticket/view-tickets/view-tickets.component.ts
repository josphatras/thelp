import { Component, OnInit, ViewChild } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { TicketService } from '../ticket.service';
import { Observable } from 'rxjs';
import * as $ from 'jquery';
import { Router } from '@angular/router';
import { MatTableDataSource, MatPaginator } from '@angular/material';

@Component({
  selector: 'app-view-tickets',
  templateUrl: './view-tickets.component.html',
  styleUrls: ['./view-tickets.component.sass'],
    animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ViewTicketsComponent implements OnInit {
    
      dataSource: any;
      data_list: any;
      columnsToDisplay = ['id', 'ticket_no', 'project', 'issue', 'details', 'status', 'created_at'];
      expandedElement: any;
      serverResponse: any;
      serverData: any;
      constructor(private router: Router, private data: TicketService) { }
      @ViewChild(MatPaginator) paginator: MatPaginator;
      ngOnInit() {
        this.data.getTickets().subscribe(
                    (val) => {
                        this.serverData = val;
                        this.data_list = Object.keys(this.serverData).map(e=>this.serverData[e]);
                        this.dataSource = new MatTableDataSource(this.data_list);
                        this.dataSource.paginator = this.paginator;
            },
            response => {

            },
            () => {
                console.log("The POST observable is now completed.");
              }
            )
      }
      getTicket(ticket){
        this.router.navigate(['/ticket/'+ ticket]);
      }
}
