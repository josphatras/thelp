import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { TicketService } from '../ticket.service';
import { Observable } from 'rxjs';
import * as $ from 'jquery';
import { Router } from '@angular/router';

@Component({
  selector: 'app-assign-ticket',
  templateUrl: './assign-ticket.component.html',
  styleUrls: ['./assign-ticket.component.sass']
})
export class AssignTicketComponent implements OnInit {
    // priorities: Object;
    // issues: Object;
    // pms: Object;
    priorities: any;
    issues: any;
    pms: any;

		projectId: any;
	  serverResponse: any;

    AssignTicketForm = new FormGroup({
        priority: new FormControl(''),
        assign_to: new FormControl(''),
        description: new FormControl(''),
        ticket_id: new FormControl(''),
    });

  constructor(
    public dialogRef: MatDialogRef<AssignTicketComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dataR: TicketService,
    private router: Router, 
  	) { }

  ngOnInit() {
    this.AssignTicketForm.patchValue({
      ticket_id: this.data.ticket_id
    });
  	this.projectId = this.data.project_id;
    this.dataR.projectSbus(this.projectId).subscribe(
      val => {this.pms = val}
    )
    this.dataR.getPriorities().subscribe(
      val => {this.priorities = val}
    )
    this.dataR.getIssues().subscribe(
      val => {this.issues = val}
    )
  }

  closeAssign(): void {
    this.dialogRef.close();
  }
  assignTicket() {
    $('.assignLoad').addClass('fa fa-refresh fa-spin');
    this.dataR.assignTicket(this.AssignTicketForm.value).subscribe(
      (val) => {
        this.serverResponse = val
        if (this.serverResponse.status == 'success') {
          setTimeout(() => {
            $('.assignLoad').removeClass('fa fa-refresh fa-spin');
            $('.response').html('<div class="alert alert-success sucess-response">'+ this.serverResponse.msg.response +'</div>');
            $('.sucess-response').fadeOut(3000);
          }, 500);
          setTimeout(() => {
            this.closeAssign();
            this.router.navigate(['/ticket/'+ this.data.ticket_id]);
          }, 500);

        }else if(this.serverResponse.status == 'fail'){
          setTimeout(() => {
            $('.assignLoad').removeClass('fa fa-refresh fa-spin');
            $('.response').html('<div class="alert alert-danger fail-response">'+ this.serverResponse.msg.response +'</div>');
            $('.fail-response').fadeOut(3000);
          }, 500); 
        }
      },
      response => {
          setTimeout(() => {
            $('.assignLoad').removeClass('fa fa-refresh fa-spin');
            $('.response').html('<div class="alert alert-danger fail-response">Something went wrong</div>');
            $('.fail-response').fadeOut(3000);
          }, 500); 
      },
      () => {
          //console.log("The POST observable is now completed.");
        }
    )
  }

}
