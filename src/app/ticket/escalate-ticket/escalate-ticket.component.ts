import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { TicketService } from '../ticket.service';
import { Observable } from 'rxjs';
import * as $ from 'jquery';
import { Router } from '@angular/router';

@Component({
  selector: 'app-escalate-ticket',
  templateUrl: './escalate-ticket.component.html',
  styleUrls: ['./escalate-ticket.component.sass']
})
export class EscalateTicketComponent implements OnInit {
    // priorities: Object;
    // issues: Object;
    // pms: Object;
    priorities: any;
    issues: any;
    pms: any;
    serverResponse: any;

    EscalateTicketForm = new FormGroup({
        priority: new FormControl(''),
        category: new FormControl(''),
        escalte_to: new FormControl(''),
        description: new FormControl(''),
        ticket_id: new FormControl(''),
    });

  constructor(
    public dialogRef: MatDialogRef<EscalateTicketComponent>,
    @Inject(MAT_DIALOG_DATA) public dataI: any,
    private data: TicketService,
    private router: Router, 
    ) { }

  ngOnInit() {
    this.EscalateTicketForm.patchValue({
      ticket_id: this.dataI.ticket_id
    });

    this.data.getPms().subscribe(
      data => {this.pms = data}
    )
    this.data.getPriorities().subscribe(
      data => {this.priorities = data}
    )
    this.data.getIssues().subscribe(
      data => {this.issues = data}
    )
  }

  closeEsc(): void {
    this.dialogRef.close();
  }

  EscalateTicket() {
    $('.escalateLoad').addClass('fa fa-refresh fa-spin');
    var items = this.EscalateTicketForm.value;
    var userGroup = items.userGroup;
    var completeForm = true;
    if (items.escalte_to == '' || items.description == '') {
        completeForm = false;
      }
​
    if (completeForm) {
      this.data.escalateTicket(items).subscribe(
        (val) => {
          this.serverResponse = val
          if (this.serverResponse.status == 'success') {
            setTimeout(() => {
              $('.escalateLoad').removeClass('fa fa-refresh fa-spin');
              $('.response').html('<div class="alert alert-success sucess-response">'+ this.serverResponse.msg.response +'</div>');
              $('.sucess-response').fadeOut(3000);
            }, 500);
            setTimeout(() => {
              this.closeEsc();
              this.router.navigate(['/ticket/' + items.ticket_id]);
            }, 3500);

          }else if(this.serverResponse.status == 'fail'){
            setTimeout(() => {
              $('.escalateLoad').removeClass('fa fa-refresh fa-spin');
              $('.response').html('<div class="alert alert-danger fail-response">'+ this.serverResponse.msg.response +'</div>');
              $('.fail-response').fadeOut(3000);
            }, 500); 
          }
        },
        response => {
            setTimeout(() => {
              $('.escalateLoad').removeClass('fa fa-refresh fa-spin');
              $('.response').html('<div class="alert alert-danger fail-response">Something went wrong</div>');
              $('.fail-response').fadeOut(3000);
            }, 500); 
        },
        () => {
            //console.log("The POST observable is now completed.");
          }
      )
    }else{
      setTimeout(() => {
        $('.escalateLoad').removeClass('fa fa-refresh fa-spin');
        $('.response').html('<div class="alert alert-danger fail-response"> One or more empty fields</div>');
        $('.fail-response').fadeOut(3000);
      }, 500);
    }
  }
}