import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { TicketService } from '../ticket.service';
import { Observable } from 'rxjs';
import * as $ from 'jquery';
import { Router } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-edit-ticket',
  templateUrl: './edit-ticket.component.html',
  styleUrls: ['./edit-ticket.component.sass']
})
export class EditTicketComponent implements OnInit {
	// projects: Object;
	// priorities: Object;
	// issues: Object;
  projects: any;
  priorities: any;
  issues: any;
  serverResponse: any;
  ticket: any;

	EditTicketForm = new FormGroup({
		project: new FormControl(''),
		priority: new FormControl(''),
		issue: new FormControl(''),
		details: new FormControl(''),
	});

  // editform: EditForm = new EditForm();

  constructor(
    private router: Router, 
    private data: TicketService,
    @Inject(MAT_DIALOG_DATA) public info: any,
    public dialogRef: MatDialogRef<EditTicketComponent>,
  ) { }

  ngOnInit() {
    this.ticket = this.info.ticket;
    this.EditTicketForm.controls['project'].setValue(this.ticket.project);
    this.EditTicketForm.controls['priority'].setValue(this.ticket.priority);
    this.EditTicketForm.controls['issue'].setValue(this.ticket.issue);
    this.EditTicketForm.controls['details'].setValue(this.ticket.details);

  	this.data.getProjects().subscribe(
  		data => {this.projects = data}
  	)
  	this.data.getPriorities().subscribe(
  		data => {this.priorities = data}
  	)
  	this.data.getIssues().subscribe(
  		data => {this.issues = data}
  	)  	
  }

  editTicket() {
    $('.editLoad').addClass('fa fa-refresh fa-spin');
    var items = this.EditTicketForm.value;
    var userGroup = items.userGroup;
    var completeForm = true;
    if (items.project == '' || items.priority == '' || items.issue == '' || items.details == '') {
        completeForm = false;
      }
​
    if (completeForm) {
      this.data.editTicket(items).subscribe(
        (val) => {
          this.serverResponse = val
          if (this.serverResponse.status == 'success') {
            setTimeout(() => {
              $('.editLoad').removeClass('fa fa-refresh fa-spin');
              $('.response').html('<div class="alert alert-success sucess-response">'+ this.serverResponse.msg.response +'</div>');
              $('.sucess-response').fadeOut(3000);
            }, 500);
            setTimeout(() => {
              this.router.navigate(['/ticket/'+ this.serverResponse.msg.ticket_id]);
            }, 500);

          }else if(this.serverResponse.status == 'fail'){
            setTimeout(() => {
              $('.editLoad').removeClass('fa fa-refresh fa-spin');
              $('.response').html('<div class="alert alert-danger fail-response">'+ this.serverResponse.msg.response +'</div>');
              $('.fail-response').fadeOut(3000);
            }, 500); 
          }
        },
        response => {
            setTimeout(() => {
              $('.editLoad').removeClass('fa fa-refresh fa-spin');
              $('.response').html('<div class="alert alert-danger fail-response">Something went wrong</div>');
              $('.fail-response').fadeOut(3000);
            }, 500); 
        },
        () => {
            //console.log("The POST observable is now completed.");
          }
      )
    }else{
      setTimeout(() => {
        $('.editLoad').removeClass('fa fa-refresh fa-spin');
        $('.response').html('<div class="alert alert-danger fail-response"> One or more empty fields</div>');
        $('.fail-response').fadeOut(3000);
      }, 500);
    }  	
  }

}

export class EditForm{
  project: any;
  priority: any;
  issue: any;
  details: any;
}