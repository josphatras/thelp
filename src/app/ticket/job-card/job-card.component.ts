import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { TicketService } from '../ticket.service';
import { Observable } from 'rxjs';
import * as $ from 'jquery';
import { Router } from '@angular/router';

@Component({
  selector: 'app-job-card',
  templateUrl: './job-card.component.html',
  styleUrls: ['./job-card.component.sass']
})
export class JobCardComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<JobCardComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dataR: TicketService,
    private router: Router, 
  ) { }

  ngOnInit() {

  }

}
