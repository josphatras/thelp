import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TicketService } from '../ticket.service';
import { EscalateTicketComponent } from '../escalate-ticket/escalate-ticket.component';
import { EditTicketComponent } from '../edit-ticket/edit-ticket.component';
import { AssignTicketComponent } from '../assign-ticket/assign-ticket.component';
import { DeleteTicketComponent } from '../delete-ticket/delete-ticket.component';
import { JobCardComponent } from '../job-card/job-card.component';
import { interval, Observable } from 'rxjs';
import * as $ from 'jquery';
import * as Pusher from '../../../assets/js/pusher.min.js';

import  { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-view-ticket',
	templateUrl: './view-ticket.component.html',
	styleUrls: ['./view-ticket.component.sass'],
	animations: [
		trigger('detailExpand', [
			state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
			state('expanded', style({height: '*'})),
			transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
		]),
	],
})
export class ViewTicketComponent implements OnInit {
		serverResponse: any;
		chatItems: any;
		chats: any;
		ticket: any;
		ticket_id: any;
		project_id: any;
		status: any = [];
		sts: any;
		serverData: any;
		user: any;
		supportHrs: boolean = false;
		supportMins: boolean = false;
		details: boolean = false;
		ticketInput: boolean = false;

		edit_btn: boolean = false;
		assgn_btn: boolean = false;
		esc_btn: boolean = false;
		delete_btn: boolean = false;
		assg_section: boolean = false;
		job_card_btn: boolean = false;

		UpdateTicketFormValid: boolean = true;

		UpdateTicketForm = new FormGroup({
			status: new FormControl(''),
			hrs: new FormControl(''),
			mins: new FormControl(''),
			details: new FormControl(''),
		});

		TicketChatForm = new FormGroup({
			msg: new FormControl(''),
			ticket_id: new FormControl(''),
			chat_category: new FormControl('4'),
		});


		constructor(
			private route: ActivatedRoute,
			private data: TicketService,
			public dialog: MatDialog,
			// public socket: Socket,
		) { }

	ngOnInit() {
		this.user = JSON.parse(localStorage.getItem("tbl_user"));
		if (this.user.role == '2' || this.user.role == '3' || this.user.role == '9') {
			this.edit_btn = false;
			this.assgn_btn = false;
			this.esc_btn = false;
			this.delete_btn = false;
		}else if(this.user.role == '1' || this.user.role == '8'){
			this.edit_btn = true;
			this.assgn_btn = true;
			this.esc_btn = true;
			this.delete_btn = true;
			this.assg_section = true;
		}else{
			this.assg_section = true;
		}

		this.route.params.subscribe(params=>{
			this.ticket_id = params['option']
			this.data.getTicket(this.ticket_id).subscribe(
				(val) => {
						this.ticket = val;
						// console.log(this.ticket.ticket_escalations);
						this.project_id = this.ticket.project_id;
						if (this.ticket.status_id == '6') {
							this.UpdateTicketFormValid = false;
							this.job_card_btn = true;
						}else{
							this.UpdateTicketFormValid = true;
							this.job_card_btn = false;
						}
						this.UpdateTicketForm.addControl('ticket_no', new FormControl(this.ticket.id));
						this.TicketChatForm.patchValue({
							ticket_id: this.ticket.id
						});
						this.data.getStatus().subscribe( val => {
								this.serverData = val
								this.status = val
								if (this.ticket.status_id == 6){
									this.status = [{'id': 8, 'name': 'Reopen', 'section': 'ticket'}]
								}
							});
						this.viewChat();
						this.chatListener(this.ticket.id);
						// const source = interval(300000);
						// source.subscribe(val => {
						// 	this.viewChat();
						// });
				},
				response => {

				},
				() => {
						// console.log("The POST observable is now completed.");
			})
		});
	}

	statusChange(){
		var formInputs = this.UpdateTicketForm.value;
		var status = formInputs.status;
		if (status == '6') {
			this.supportHrs = true;
			this.supportMins = true;
			this.details = true;
		}else if(status == '3' || status == '4' || status == '5' || status == '8'){
			this.supportHrs = false;
			this.supportMins = false;
			this.details = true;
		}else{
			this.supportHrs = false;
			this.supportMins = false;
			this.details = false;
		}
	}


	onSubmit(){
		$('.submitLoad').addClass('fa fa-refresh fa-spin');
		var items = this.UpdateTicketForm.value;
		var status = items.status;
		var completeForm = true;

		if (items.status == '') {
				completeForm = false;
			}
		if (items.status == '4' || items.status == '5' || items.status == '6') {
			if (items.hrs > 1 || items.mins > 1 || items.details !== '') {
				 completeForm = true;
			}else{
				completeForm = false;
			} 
		}else if (items.status == '8') {
			if (items.details == '') {
				 completeForm = false;
			}
		}

		if (completeForm) {
			// this.UpdateTicketForm.get('ticket_no').setValue(this.ticket.id);
			this.data.updateTicket(items).subscribe(
				(val) => {
					this.serverResponse = val
					if (this.serverResponse.status == 'success') {
						this.UpdateTicketForm.removeControl('ticket_no')
						setTimeout(() => {
							$('.submitLoad').removeClass('fa fa-refresh fa-spin');
							$('.response').html('<div class="alert alert-success sucess-response">'+ this.serverResponse.msg.response +'</div>');
							$('.sucess-response').fadeOut(2000);
						}, 500);
						setTimeout(() => {
							this.ngOnInit();
							this.UpdateTicketForm.reset();
						}, 2500);

					}else if(this.serverResponse.status == 'fail'){
						setTimeout(() => {
							$('.submitLoad').removeClass('fa fa-refresh fa-spin');
							$('.response').html('<div class="alert alert-danger fail-response">'+ this.serverResponse.msg.response +'</div>');
							$('.fail-response').fadeOut(3000);
						}, 500); 
					}
				},
				response => {
						setTimeout(() => {
							$('.submitLoad').removeClass('fa fa-refresh fa-spin');
							$('.response').html('<div class="alert alert-danger fail-response">Something went wrong</div>');
							$('.fail-response').fadeOut(3000);
						}, 500); 
				},
				() => {
						console.log("The POST observable is now completed.");
					}
			)
		}else{
			setTimeout(() => {
				$('.submitLoad').removeClass('fa fa-refresh fa-spin');
				$('.response').html('<div class="alert alert-danger fail-response"> One or more empty fields</div>');
				$('.fail-response').fadeOut(3000);
			}, 500);
		}
	}


	popEscalate(): void {
		const dialogRef = this.dialog.open(EscalateTicketComponent,{
			width: '600px',
			data: { ticket_no: this.ticket.ticket_no, ticket_id: this.ticket_id }
		});

		dialogRef.afterClosed().subscribe(result => {
			console.log('The dialog was closed');
		});
	}

	popEdit(ticketdata): void {
		const dialogRef = this.dialog.open(EditTicketComponent,{
			width: '500px',
			data: {ticket: ticketdata}});

		dialogRef.afterClosed().subscribe(result => {
			// console.log('The dialog was closed');
			this.ngOnInit();
		});
	}

	popAssign(): void {
		const dialogRef = this.dialog.open(AssignTicketComponent,{
			width: '500px',
			data: {project_id: this.project_id, ticket_id: this.ticket_id}
		});

		dialogRef.afterClosed().subscribe(result => {
			// console.log('The dialog was closed');
			this.ngOnInit();
		});
	}
	
	popDelete(ticket_id): void {
		const dialogRef = this.dialog.open(DeleteTicketComponent,{
			width: '500px',
			data: {ticketid: ticket_id}
		});

		dialogRef.afterClosed().subscribe(result => {
			// console.log('The dialog was closed');
		});
	}

	popJobCard(){
		const dialogRef = this.dialog.open(JobCardComponent,{
			data: {ticket: this.ticket}
		});

		dialogRef.afterClosed().subscribe(result => {
			// console.log('The dialog was closed');
		});		
	}

 chatSubmit(){
	$('.chatLoad').addClass('fa fa-refresh fa-spin');
	var items = this.TicketChatForm.value;
		this.data.newMessage(items).subscribe(
			(val) => {
				this.chatItems = val
				if (this.chatItems.status == 'success') {
						this.UpdateTicketForm.removeControl('ticket_no')
						$('.chatLoad').removeClass('fa fa-refresh fa-spin');
						this.viewChat();
				}else if(this.chatItems.status == 'fail'){
						$('.submitLoad').removeClass('fa fa-refresh fa-spin');
						$('.chatResponse').html('<div class="alert alert-danger fail-response">'+ this.chatItems.msg.response +'</div>');
						$('.fail-response').fadeOut(3000);
				}
			},
			response => {
					setTimeout(() => {
						$('.submitLoad').removeClass('fa fa-refresh fa-spin');
						$('.chatResponse').html('<div class="alert alert-danger fail-response">Something went wrong</div>');
						$('.fail-response').fadeOut(3000);
					}, 500); 
			},
			() => {
					console.log("The POST observable is now completed.");
				}
		)
 } 
	viewChat(){
			this.data.viewChat('4', this.ticket.id).subscribe(
				(val) => {
					this.chats = val;
						this.TicketChatForm.patchValue({
							msg: ''
						});
					if (this.chats.status == 'success') {
							// $('.response').html('<div class="alert alert-success sucess-response">'+ this.chats.msg.response +'</div>');
							// $('.sucess-response').fadeOut(2000);

					}else if(this.chats.status == 'fail'){
						$('.submitLoad').removeClass('fa fa-refresh fa-spin');
						$('.response').html('<div class="alert alert-danger fail-response">'+ this.chats.msg.response +'</div>');
					}
				},
				response => {
					$('.submiLoad').removeClass('fa fa-refresh fa-spin');
					$('.response').html('<div class="alert alert-danger fail-response">Something went wrong</div>');
				},
				() => {
						console.log("The POST observable is now completed.");
					}
			)
	}

	chatListener(ticket_id){
		console.log('listen_to ' + ticket_id)
    var pusher = new Pusher('3271aff7048681caa70e', {
      cluster: 'ap2',
      forceTLS: true
    });
    // Subscribe to the channel we specified in our Laravel Event
    var channel = pusher.subscribe('4' + ticket_id);

    // Bind a function to a Event (the full Laravel class)

  	const source = this.viewChat();
    channel.bind('chatting', function(data) {	
    	console.log(data);
					// this.UpdateTicketForm.removeControl('ticket_no')
					// $('.chatLoad').removeClass('fa fa-refresh fa-spin');
    	// this.source;
		});
	}
}
