import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SysConfig } from '../config/sys';

@Injectable({
  providedIn: 'root'
})
export class TicketService {
	hearderOptions: any
	generatedHeader: any
	token: any

	authKey(){
		this.token = JSON.parse(localStorage.getItem('tbl_token'))
		return this.token
	}

	genHeders(token_key){
		this.generatedHeader = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json', 
				'Access-Control-Allow-Origin': '*', 
				'Authorization' : token_key,
			})
		}
		return this.generatedHeader;
	}
  private config = SysConfig.siteUrl; 

	readonly ROOT_URL = this.config+'api/tickets/create';
	readonly UPDATE_TICKET_URL = this.config+'api/tickets/update';
	readonly EDIT_TICKET_URL = this.config+'api/tickets/edit';
	readonly ESCALATE_TICKET_URL = this.config+'api/tickets/escalate';
	readonly DELETE_TICKET_URL = this.config+'api/tickets/delete';
	readonly ASSIGN_TICKET_URL = this.config+'api/tickets/assign';
	readonly NEW_MSG_URL = this.config+'api/chat/new_msg';
	readonly CHAT_VIEW_URL = this.config+'api/chat/view';

	constructor(private http: HttpClient) { }

	getTickets(){
	  var info = this.http.get(this.config+'api/tickets/show', this.genHeders('Bearer ' + this.authKey()));  
	  return info;
	}
	getTicket(ticket_id){
	  var info = this.http.get(this.config+'api/ticket/' + ticket_id, this.genHeders('Bearer ' + this.authKey()));  
	  return info;    
	}
	getProjects(){
		var info = this.http.get(this.config+'api/items/project', this.genHeders('Bearer ' + this.authKey()));  
		return info;
	}
	getPriorities(){
		var info = this.http.get(this.config+'api/items/priority', this.genHeders('Bearer ' + this.authKey()));  
		return info;
	}
	getIssues(){
		var info = this.http.get(this.config+'api/items/issue', this.genHeders('Bearer ' + this.authKey()));
		return info;
	}
	getStatus(){
		var info = this.http.get(this.config+'api/status/ticket', this.genHeders('Bearer ' + this.authKey()));
		return info;
	}
	createTicket(ticketData){
		let url = this.ROOT_URL;
		this.hearderOptions = this.genHeders('Bearer ' + this.authKey());
		return this.http.post(url, ticketData , this.hearderOptions);
	}
	updateTicket(items){
		let url = this.UPDATE_TICKET_URL;
		this.hearderOptions = this.genHeders('Bearer ' + this.authKey());
		return this.http.post(url, items , this.hearderOptions);    
	}

	getPms(){
	  var info = this.http.get(this.config+'api/users/role/6', this.genHeders('Bearer ' + this.authKey()));  
	  return info;  
	}
	projectSbus(projectId){
	  var info = this.http.get(this.config+'api/users/project-sbus/' + projectId, this.genHeders('Bearer ' + this.authKey()));  
	  return info; 
	}
	escalateTicket(escData){
		let url = this.ESCALATE_TICKET_URL;
		this.hearderOptions = this.genHeders('Bearer ' + this.authKey());
		return this.http.post(url, escData , this.hearderOptions);           
	}

	editTicket(ticketData){
		let url = this.EDIT_TICKET_URL;
		this.hearderOptions = this.genHeders('Bearer ' + this.authKey());
		return this.http.post(url, ticketData , this.hearderOptions);               
	}

	deleteTicket(ticketId){
	 let url = this.DELETE_TICKET_URL;
		this.hearderOptions = this.genHeders('Bearer ' + this.authKey());
		return this.http.post(url, {'ticket': ticketId} , this.hearderOptions);       
	}

	assignTicket(details){
	 let url = this.ASSIGN_TICKET_URL;
		this.hearderOptions = this.genHeders('Bearer ' + this.authKey());
		return this.http.post(url, details , this.hearderOptions);       
	}

	newMessage(details){
	 let url = this.NEW_MSG_URL;
		this.hearderOptions = this.genHeders('Bearer ' + this.authKey());
		return this.http.post(url, details , this.hearderOptions);  		
	}
	
	viewChat(item, ticket_id){
	 let url = this.CHAT_VIEW_URL;
	  var info = this.http.get(url + '/' + item + '/' + ticket_id, this.genHeders('Bearer ' + this.authKey()));  
	  return info; 	
	}
}
