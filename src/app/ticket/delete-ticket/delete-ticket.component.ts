import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { TicketService } from '../ticket.service';
import { Observable } from 'rxjs';
import * as $ from 'jquery';
import { Router } from '@angular/router';

@Component({
  selector: 'app-delete-ticket',
  templateUrl: './delete-ticket.component.html',
  styleUrls: ['./delete-ticket.component.sass']
})
export class DeleteTicketComponent implements OnInit {

	ticketId: any;
  serverResponse: any;
  constructor(
    public dialogRef: MatDialogRef<DeleteTicketComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dataR: TicketService,
    private router: Router, 
  ) {}

  ngOnInit() {
  	this.ticketId = this.data.ticketid;
  }
  
  closeDelete(): void {
    this.dialogRef.close();
  }

  deleteTicket(ticketId){
    $('.deleteLoad').addClass('fa fa-refresh fa-spin');
    this.dataR.deleteTicket(ticketId).subscribe(
      (val) => {
        this.serverResponse = val
        if (this.serverResponse.status == 'success') {
            setTimeout(() => {
              $('.deleteLoad').removeClass('fa fa-refresh fa-spin');
              $('.deleteResponse').html('<div class="alert alert-success sucess-response">'+ this.serverResponse.msg.response +'</div>');
              $('.sucess-response').fadeOut(3000);
            }, 500);
            setTimeout(() => {
              this.closeDelete();
              this.router.navigate(['/tickets/view']);
            }, 3500);
        }else if(this.serverResponse.status == 'fail'){
            setTimeout(() => {
              $('.submitLoad').removeClass('fa fa-refresh fa-spin');
              $('.response').html('<div class="alert alert-danger fail-response">'+ this.serverResponse.msg.response +'</div>');
              $('.fail-response').fadeOut(3000);
            }, 500); 
          }

      }
    )
  }
}
