import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceTrackerReportComponent } from './service-tracker-report.component';

describe('ServiceTrackerReportComponent', () => {
  let component: ServiceTrackerReportComponent;
  let fixture: ComponentFixture<ServiceTrackerReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceTrackerReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceTrackerReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
