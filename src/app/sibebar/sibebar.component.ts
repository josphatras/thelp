import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ChangePasswordComponent } from '../account/change-password/change-password.component';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SysConfig } from '../config/sys';
import * as $ from 'jquery';
import * as Pusher from '../../assets/js/pusher.min.js';
import * as moment from 'moment';


@Component({
  selector: 'app-sibebar',
  templateUrl: './sibebar.component.html',
  styleUrls: ['./sibebar.component.sass']
})
export class SibebarComponent implements OnInit {

  notificationCount: any
  notificationText: any  
	userName: any
	user: any
	hearderOptions: any
	generatedHeader: any
	token: any
  notItems: any

  constructor(
  	private router: Router,
		private dialog: MatDialog,
		private http: HttpClient,
  ) { }

  private config = SysConfig.siteUrl; 
	authKey(){
		this.token = JSON.parse(localStorage.getItem('tbl_token'))
		return this.token
	}

	genHeders(token_key){
		this.generatedHeader = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json', 
				'Access-Control-Allow-Origin': '*', 
				'Authorization' : token_key,
			})
		}
		return this.generatedHeader;
	}

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem("tbl_user"));
    if (this.user !== null) {
      this.user = JSON.parse(localStorage.getItem("tbl_user"));
  		this.userNotifications(this.user.id).subscribe(
  			data => {
          var notificationsWrapper   = $('.dropdown-notifications');
          var notificationsToggle    = notificationsWrapper.find('a[data-toggle]');
          var notificationsCountElem = notificationsToggle.find('i[data-count]');
          var notificationsCount     = parseInt(notificationsCountElem.data('count'));
          var notifications          = notificationsWrapper.find('ul.dropdown-menu');
          this.notItems = data;
          var ntfs = Object.keys(data).map(e=>data[e]);

          for (let notification of ntfs) {
            if (notification.viewed == '0') {
              notificationsCount +=1;
            }
            var existingNotifications = notifications.html();
            var avatar = Math.floor(Math.random() * (71 - 20 + 1)) + 20;
            var timestamp = new Date().getTime();
            if (notification.item == "1") {
              var img = "user-icon.png";
              var link = '/#/users/view';
            }else if(notification.item == "2"){
              var img = "client-icon.png";
              var link = '/#/clients/view';
            }else if(notification.item == "3"){
              var img = "project-icon.png";
              var link = '/#/projects/show/active';
            }else if(notification.item == "4"){
              var img = "ticket-icon.png";
              var link = '/#/ticket/'+notification.item_id;
            }

            var newNotificationHtml = 
            '<li class="notification active">'+
                  '<div class="media">'+
                    '<div class="media-left">'+
                      '<div class="media-object">'+
                      '<img src="assets/images/' + img + '" width="50px">'+
                      '</div>'+
                    '</div>'+
                    '<div class="media-body" (click)="notNavigation()">'+
                      '<a href="' + link + '">'+
                      '<strong class="notification-title">' + notification.notification_message + '</strong>'+
                      '</a>'+
                      '<!--p class="notification-desc">Extra description can go here</p-->'+
                      '<div class="notification-meta">'+
                        '<small class="timestamp">' + moment(notification.created_at).calendar() + '</small>'+
                      '</div>'+
                    '</div>'+
                  '</div>'+
              '</li>';
            notifications.html(newNotificationHtml + existingNotifications);
          }

          notificationsCountElem.attr('data-count', notificationsCount);
          notificationsWrapper.find('.notif-count').text(notificationsCount);
          notificationsWrapper.show();
  			}
  		)
    	var res = this.user.full_name.split(" ");
    	this.userName = 'Hi ' + res['0'] + '!';
      switch (this.user.role) {
        case 1:
          $('.tbl_admin').show();
          break;
        case 2:
          $('.cl_admin').show();
          break;
        case 3:
          $('.cl_con').show();
          break;
        case 4:
          $('.tbl_sbu_dir').show();
          break;
        case 5:
          $('.tbl_sbu_psh').show();
          break;
        case 6:
          $('.tbl_pro_mg').show();
          break;
        case 7:
          $('.cl_unkown').show();
          break;
        case 8:
          $('.tbl_super_admin').show();
          break;
        case 9:
           $('.cl_super_admin').show();
          break;
        default:
          break;
      }
		}
    this.notificationsChannels();

  }
  notSeen(userId){
    console.log(userId);
    var notificationsWrapper   = $('.dropdown-notifications');
    var notificationsToggle    = notificationsWrapper.find('a[data-toggle]');
    var notificationsCountElem = notificationsToggle.find('i[data-count]');
    var notificationsCount     = parseInt(notificationsCountElem.data('count'));
    notificationsCountElem.attr('data-count', '0');
    notificationsWrapper.find('.notif-count').text('0');
    this.seen(userId).subscribe(
    data => {

    })
  }

  seen(userId){
    let url = this.config+'api/notifications/seen/' + userId;
    var info = this.http.get(url, this.genHeders('Bearer ' + this.authKey()));  
    return info;
  }

  notificationsChannels(){
/*    var moment = require('moment-timezone');
    moment().tz("Africa/Nairobi").format();*/
    var notificationsWrapper   = $('.dropdown-notifications');
    var notificationsToggle    = notificationsWrapper.find('a[data-toggle]');
    var notificationsCountElem = notificationsToggle.find('i[data-count]');
    var notificationsCount     = parseInt(notificationsCountElem.data('count'));
    var notifications          = notificationsWrapper.find('ul.dropdown-menu');
    // Pusher.logToConsole = true;

    var pusher = new Pusher('3271aff7048681caa70e', {
      cluster: 'ap2',
      forceTLS: true
    });

    // Subscribe to the channel we specified in our Laravel Event
    var user = JSON.parse(localStorage.getItem("tbl_user"));
    var channel = pusher.subscribe('user' + user.id);

    // Bind a function to a Event (the full Laravel class)
    channel.bind('notification', function(data) {
      // console.log(data);
      var existingNotifications = notifications.html();
      var avatar = Math.floor(Math.random() * (71 - 20 + 1)) + 20;

      if (data.item == "1") {
        var img = "user-icon.png";
        var link = '/#/users/view';
      }else if(data.item == "2"){
        var img = "client-icon.png";
        var link = '/#/clients/view';
      }else if(data.item == "3"){
        var img = "project-icon.png";
        var link = '/#/clients/view';
      }else if(data.item == "4"){
        var img = "ticket-icon.png";
        var link = '/#/ticket/'+data.item_id;
      }

      var newNotificationHtml =
              '<li class="notification active">'+
                  '<div class="media">'+
                    '<div class="media-left">'+
                      '<div class="media-object">'+
                        '<img src="assets/images/' + img + '" width="50px">'+
                      '</div>'+
                    '</div>'+
                    '<div class="media-body" (click)="notNavigation()">'+
                      '<a href="' + link + '">'+
                      '<strong class="notification-title">' + data.msg + '</strong>'+
                      '</a>'+
                      '<!--p class="notification-desc">Extra description can go here</p-->'+
                      '<div class="notification-meta">'+
                        '<small class="timestamp">' + moment(data.created_at).calendar() + '</small>'+
                      '</div>'+
                    '</div>'+
                  '</div>'+
              '</li>';
      notifications.html(newNotificationHtml + existingNotifications);
      notificationsCount += 1;
      notificationsCountElem.attr('data-count', notificationsCount);
      notificationsWrapper.find('.notif-count').text(notificationsCount);
    });    
      // notificationsWrapper.show();
  }

  changePassword(userId): void {
    const dialogRef = this.dialog.open(ChangePasswordComponent,{
      width: '500px',
      data: {id: userId}
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
    });
  }

  userNotifications(userId){
    $('.notif-count').html('200');
    // console.log('Here!');
    let url = this.config+'api/notifications/view/' + userId;
    var info = this.http.get(url, this.genHeders('Bearer ' + this.authKey()));  
    return info;
  }

  notNavigation(){
    // console.log('here');
    this.router.navigate(['/projects/show/active']);
  }

}
