import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { SbuRoutingModule } from './sbu/sbu-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { SibebarComponent } from './sibebar/sibebar.component';
import { ViewSbuComponent } from './sbu/view-sbu/view-sbu.component';
import { UserLoginComponent } from './account/user-login/user-login.component';
import { UserLogoutComponent } from './account/user-logout/user-logout.component';
import { NewUserComponent } from './account/new-user/new-user.component';
import { SelectUsersComponent } from './account/select-users/select-users.component';
import { ViewUserComponent } from './account/view-user/view-user.component';

import { EditUserComponent } from './account/edit-user/edit-user.component';
import { DeleteUserComponent } from './account/delete-user/delete-user.component';
import { ResetPasswordComponent } from './account/reset-password/reset-password.component';
import { ChangePasswordComponent } from './account/change-password/change-password.component';

import { NewProjectComponent } from './project/new-project/new-project.component';
import { DeleteProjectComponent } from './project/delete-project/delete-project.component';
import { EditProjectComponent } from './project/edit-project/edit-project.component';
import { ViewProjectsComponent } from './project/view-projects/view-projects.component';

import { NewClientComponent } from './client/new-client/new-client.component';
import { ViewClientComponent } from './client/view-client/view-client.component';
import { EditClientComponent } from './client/edit-client/edit-client.component';
import { DeleteClientComponent } from './client/delete-client/delete-client.component';

import { NewTicketComponent } from './ticket/new-ticket/new-ticket.component';
import { ViewTicketsComponent } from './ticket/view-tickets/view-tickets.component';
import { ViewTicketComponent } from './ticket/view-ticket/view-ticket.component';
import { EscalateTicketComponent } from './ticket/escalate-ticket/escalate-ticket.component';
import { EditTicketComponent } from './ticket/edit-ticket/edit-ticket.component';
import { AssignTicketComponent } from './ticket/assign-ticket/assign-ticket.component';
import { DeleteTicketComponent } from './ticket/delete-ticket/delete-ticket.component';
import { JobCardComponent } from './ticket/job-card/job-card.component';
import { MonthlyReportComponent } from './ticket/monthly-report/monthly-report.component';
import { ServiceTrackerReportComponent } from './ticket/service-tracker-report/service-tracker-report.component';


import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatBadgeModule } from '@angular/material/badge';
import { MatChipsModule } from '@angular/material/chips';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatToolbarModule } from '@angular/material/toolbar'; 

import { HashLocationStrategy, LocationStrategy } from '@angular/common';

// import { SocketIoModule, SocketIoConfig } from 'socket.io';

// const config: SocketIoConfig = { url: 'http://localhost:3000', options: {} };

@NgModule({
  declarations: [
    AppComponent,
    NewUserComponent,
    SelectUsersComponent,
    ViewSbuComponent,
    SibebarComponent,
    NewProjectComponent,
    ViewProjectsComponent,
    NewClientComponent,
    ViewClientComponent,
    EditClientComponent,
    DeleteClientComponent,
    ViewUserComponent,
    UserLoginComponent,
    UserLogoutComponent,
    EditUserComponent,
    DeleteUserComponent,
    ResetPasswordComponent,
    ChangePasswordComponent,
    DashboardComponent,
    NewTicketComponent,
    ViewTicketsComponent,
    ViewTicketComponent,
    EscalateTicketComponent,
    EditTicketComponent,
    AssignTicketComponent,
    DeleteTicketComponent,
    DeleteProjectComponent,
    EditProjectComponent,
    JobCardComponent,
    MonthlyReportComponent,
    ServiceTrackerReportComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    MatTableModule,
    MatListModule,
    MatIconModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatDatepickerModule,
    MatBadgeModule,
    MatChipsModule,
    MatPaginatorModule,
    MatToolbarModule,
    // SocketIoModule.forRoot(config)
  ],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    SbuRoutingModule
    ],
  bootstrap: [AppComponent],
  entryComponents: [
      EscalateTicketComponent,
      EditTicketComponent,
      AssignTicketComponent,
      DeleteTicketComponent,
      EditUserComponent,
      DeleteUserComponent,
      ResetPasswordComponent,
      ChangePasswordComponent,
      EditClientComponent,
      DeleteClientComponent,
      DeleteProjectComponent,
      EditProjectComponent,
      JobCardComponent,
  ]
})

export class AppModule { }
