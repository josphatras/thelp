import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { ViewSbuComponent } from './sbu/view-sbu/view-sbu.component';
import { UserLoginComponent } from './account/user-login/user-login.component';
import { UserLogoutComponent } from './account/user-logout/user-logout.component';
import { NewUserComponent } from './account/new-user/new-user.component';
import { SelectUsersComponent } from './account/select-users/select-users.component';
import { ViewUserComponent } from './account/view-user/view-user.component';

import { NewProjectComponent } from './project/new-project/new-project.component';
import { ViewProjectsComponent } from './project/view-projects/view-projects.component';

import { NewClientComponent } from './client/new-client/new-client.component';
import { ViewClientComponent } from './client/view-client/view-client.component';

import { NewTicketComponent } from './ticket/new-ticket/new-ticket.component';
import { ViewTicketsComponent } from './ticket/view-tickets/view-tickets.component';
import { ViewTicketComponent } from './ticket/view-ticket/view-ticket.component';
import { MonthlyReportComponent } from './ticket/monthly-report/monthly-report.component';
import { ServiceTrackerReportComponent } from './ticket/service-tracker-report/service-tracker-report.component';

const routes: Routes = [
  {path: '', component: DashboardComponent },
  {path: 'dashboard', component: DashboardComponent },
  {path: 'login', component: UserLoginComponent },
  {path: 'logout', component: UserLogoutComponent },
  {path: 'users/view', component: SelectUsersComponent },
  {path: 'users/view/:id', component: ViewUserComponent },
  {path: 'users/create', component: NewUserComponent },
  {path: 'projects/create', component: NewProjectComponent },
  {path: 'projects/show/:option', component: ViewProjectsComponent },
  {path: 'clients/create', component: NewClientComponent },
  {path: 'clients/view', component: ViewClientComponent },
  {path: 'tickets/create', component: NewTicketComponent },
  {path: 'tickets/view', component: ViewTicketsComponent },
  {path: 'ticket/:option', component: ViewTicketComponent },
  {path: 'project-report', component: MonthlyReportComponent },
  {path: 'service-report', component: ServiceTrackerReportComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
